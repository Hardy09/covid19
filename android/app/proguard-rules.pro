#-keep  src/main/res/raw.**{*;}
#-dontshrink class com.example.socia
#-keep class com.exapmple.socia** { *; }
#-keep my.json
## Flutter wrapper
#-keep class io.flutter.app.** { *; }
#-keep class io.flutter.plugin.**  { *; }
#-keep class io.flutter.util.**  { *; }
#-keep class io.flutter.view.**  { *; }
#-keep class io.flutter.**  { *; }
#-keep class io.flutter.plugins.**  { *; }
#-dontwarn io.flutter.embedding.**

#-keep class com.example.socia.json** { *; }
#-dontobfuscate my.json
#-keeppackagenames com.example.xdtester.src.main.res.raw
#-keepdirectories  R.raw.my.json
#-keepdirectories android/app/src/main/res/raw
#-dontobfuscate
#-keep class app.src.main.res.raw.**  { *; }
#-keeppackagenames model, *.java
##-injars covid19/android/app/srs/main/res/raw/my.json
#-keep class model.java
#-keep public class app.src.main.res.raw.model
#-keep class app.src.main.res.raw.** { *; }
#-dontwarn app.src.main.res.raw.**
#-keep class  app.src.main.res.raw.my { *; }
#-keep class app.src.main.java.io.** { *; }
#-keeppackagenames app.src.main.res.raw.**
#-keeppackagenames app.src.main.res.raw.my.json
#-keeppackagenames app.src.main.res.raw.**
-keepdirectories app/src/main/res/raw
-dontshrink
