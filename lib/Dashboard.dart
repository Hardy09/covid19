import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:backdrop/backdrop.dart';
import 'package:xdtester/Charts/GaugeChart.dart';
import 'package:xdtester/Charts/game.dart';
import 'main.dart';
import 'Tab_Bar.dart';

//void main() {
//  runApp(Dashboard());
//}

class Dashboard extends StatefulWidget{
  dashboard createState() => dashboard();
}

class dashboard extends State<Dashboard> with TickerProviderStateMixin {
  TabController controller;
  int confirm=0;
  int currentIndex = 0;
  final tabs = Tabs();
  bool  flag = true;
  bool flag1 = false;
  @override
  void initState() {
    controller = TabController(length: 2,vsync: this);
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
   //int _currentIndex = 0;
   return MaterialApp(
     debugShowCheckedModeBanner: false,
     theme: ThemeData(
        primaryColor: Color(0xffB6D2EB)
     ),
     home: Scaffold(
       backgroundColor: Color(0xffB6D2EB),
       body: Column(
          children: <Widget>[
            MainFrame(controller),
            spacing(4)
          ],
       ),
     ),
   );
  }

  Widget MainFrame(TabController controller){
    return Expanded(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40),
              topRight: Radius.circular(40),
              bottomLeft: Radius.circular(40),
              bottomRight: Radius.circular(40)),
        ),
        child: Column(
          children: <Widget>[
            spacing(10),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      navigateToPage();
                    },
                      child: Image.asset('assets/Images/Home_Icon.png')),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        //Text('App Name',style: TextStyle(fontSize: 24,color: Colors.black54),),
                      ],
                    ),
                  )
                ],
              ),
            ),
            spacing(20),
            Container(
              margin: EdgeInsets.only(left: 16),
              child: Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Hello! JJ",style: TextStyle(fontSize: 20,color: Colors.black)),
                      spacing(4),
                      textCheck(),
                    ],
                  )
                ],
              ),
            ),
            spacing(16),
            Check(controller)
          ],
        ),
      ),
    );
  }

  Widget textCheck(){
    if(flag){
      return Text("How are you feeling ?",style: TextStyle(fontSize: 16,color: Colors.black));
    }else{
      return Row(
        children: <Widget>[
          Text("Not Feeling Well",style: TextStyle(fontSize: 16,color: Colors.black)),
          Image.asset('assets/Images/sad.png'),
        ],
      );
    }
  }

  Widget Check(TabController controller){
    if(flag){
     return halfPhase1();
    }else{
      return tabs.myTabBar(controller);
    }
  }


  navigateToPage(){
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => demo(position: 0,flag: true,)));
  }


  Widget halfPhase1(){
    return Column(
      children: <Widget>[
        great_sad(),
        spacing(8),
        if(flag1)
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Row(
              children: <Widget>[
                Text("Good to hear,that you are Great!, Go and Check for Updates!!!",
                    style: TextStyle(fontSize: 12,color: Colors.black)),
              ],
            ),
          ),
        spacing(16),
        live_updates(),
        spacing(8),
        Row(
          children: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(left: 16),
                          child: Text("Uttar Pradesh result's -",style: TextStyle(fontWeight: FontWeight.bold),)),
                    ],
                  ),
                  Container(
//                    color: Color(0xffB6D2EB),
                      height: 195,
                      width: 450,
                      child: Guage_Chart()
                  )
                ],
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget live_updates(){
    return Container(
      width: 320,
      height: 100,
      decoration: BoxDecoration(
        color: Color(0xffDAEDFE),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Stack(
        overflow: Overflow.clip,
        //alignment: AlignmentDirectional.center,
        children: <Widget>[
          Positioned(
              left: 15,
              top: 10,
              child: Text("Live",style: TextStyle(color: Colors.red))),
          Positioned(
              left: 10,
              top: 35,
              child: Text(" Updates for Covid-19 \n in India",
                style: TextStyle(fontWeight: FontWeight.bold))),
          Positioned(
              left: 10,
              top: 75,
              child: GestureDetector(
                  onTap: (){
                    navigateToPage2();
                  },
                  child: Image.asset('assets/Images/showButton.png'))),
          Positioned(
              left: 150,
              child: Image.asset('assets/Images/defenders.png'))
        ],
      ),
    );
  }


  navigateToPage2(){
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => demo(position: 1,flag: true,)));
  }

  Widget great_sad(){
    return Container(
      margin: EdgeInsets.only(left: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 80,
            height: 50,
            decoration: BoxDecoration(
              color: Color(0xffDAEDFE),
              borderRadius: BorderRadius.circular(20),
            ),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  flag1 = true;
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.asset('assets/Images/smile.png'),
                  Text('Great')
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 16),
            width: 80,
            height: 50,
            decoration: BoxDecoration(
              color: Color(0xffDAEDFE),
              borderRadius: BorderRadius.circular(20),
            ),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  flag = false;
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.asset('assets/Images/sad.png'),
                  Text('Not Well')
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }

}