import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:system_setting/system_setting.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:lottie/lottie.dart';
import 'package:xdtester/Dashboard.dart';
import 'package:xdtester/main.dart';

//void main() {
//  runApp(blue());
//}

class blue extends StatefulWidget{
  blue_function createState() => blue_function();
}

class blue_function extends State with TickerProviderStateMixin{
  FlutterBlue flutterBlue = FlutterBlue.instance;
  List<DeviceIdentifier> devicesList = [];
  List<DropdownMenuItem<BluetoothDevice>> items = [];
  bool flag = false;
  bool check = false;
  final player = AudioCache();
  AnimationController controller;


  Widget current = Text("Scanning.....");

  @override
  void initState() {
    super.initState();
    controller = AnimationController(vsync: this);
    flutterBlue.state.listen((event) {
      if(event == BluetoothState.off){
        jumpToSetting();
      }else if(event == BluetoothState.on){
        ScanDevices();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color(0xffB6D2EB),
        body: Builder(builder: (context) => Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40),
                topRight: Radius.circular(40),
                bottomLeft: Radius.circular(40),
                bottomRight: Radius.circular(40)),
          ),
          child: Center(
              child: Check(context),
            ),
        )),
      ),
    );
  }


  Widget Check(BuildContext context){
      return Column(
        children: [
          Lottie.asset('assets/heart-fluttering.zip',
            controller: controller,
            onLoaded: (composition){
              controller
                ..duration = Duration(seconds: 10)
                ..forward();
            },
          ),
         Results_Button(context)
        ],
      );
  }

  Widget Results_Button(BuildContext context){
      return (
          RaisedButton(
            onPressed: () {
              setState(() {
                ListOfBluetoothData(context);
              });
            },
            child: Text("CHECK RESULTS"),
          )
      );
  }

  navigateToPage(context) async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => demo(position: 1,flag: true,)));
  }



  Widget ListOfBluetoothData(BuildContext context){
    print("============================== Opening Alert Box");
      player.play('GUN_FIRE.mp3');
          showModalBottomSheet (
            context: context,
            builder: (BuildContext context){
              return Container(
                  height: 200,
                  margin: EdgeInsets.only(top: 8),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(left: 16,top: 8),
                              child: Text("Results",
                                  style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold))),
                          GestureDetector(
                              onTap: () => Navigator.pop(context),
                              child: Container(
                                  margin: EdgeInsets.only(right: 8),
                                  child: Image.asset("assets/Images/Cancel.png",width: 30,height: 30,)))
                        ],
                      ),
                      spacing(16),
                      Text("There are " +devicesList.length.toString()+ " Users Present",style: TextStyle(fontSize: 18))
                    ],
                  )
              );
            },
          );
      }

  jumpToSetting() {
    SystemSetting.goto(SettingTarget.BLUETOOTH);
  }

  ScanDevices(){
    flutterBlue.startScan(timeout: Duration(seconds: 10),scanMode: ScanMode.lowLatency,allowDuplicates: false);
    flutterBlue.scanResults.listen((results) {
      for (ScanResult r in results) {
        print('${r.advertisementData.localName} found! rssi: ${r.device.id}');
        devicesList.add(r.device.id);
      }
      print("Printing List");
      print(devicesList);
    },onDone: () {
    });
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }

}
