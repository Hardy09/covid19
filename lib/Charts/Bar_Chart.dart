import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:xdtester/Api/Fetched_Data.dart';
import 'package:xdtester/Charts/Charts_Structure_body.dart';
import '../Tab_Bar.dart';


class list_series extends StatefulWidget{
  String cityname;
  city_name name;
  list_series({this.cityname, this.name});

  @override
  list_seriesState createState() => list_seriesState();
}

class list_seriesState extends State<list_series> {
  BarChartStructure data = new BarChartStructure();
  List<BarChartStructure> desktopSalesData = new List();

  @override
  void initState() {
    CreateData();
  }

  Widget build(BuildContext context) {
    // TODO: implement build
    List<charts.Series<BarChartStructure, String>> series = [
        charts.Series(
          data: desktopSalesData,
          domainFn: (BarChartStructure sales, _) => sales.district,
          measureFn: (BarChartStructure sales, _) => sales.cause,
          id: 'MY DATA',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        )
    ];

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 16),
            child: Text(widget.cityname,style: TextStyle(color: Colors.indigo,fontSize: 18,fontWeight: FontWeight.bold),)),
        Expanded(
          child: charts.BarChart(
            series,
            animate: true,
              primaryMeasureAxis: new charts.NumericAxisSpec(
//                renderSpec: new charts.NoneRenderSpec(),
              showAxisLine: true,
              ),
              domainAxis: new charts.OrdinalAxisSpec(
                  showAxisLine: true,
//                renderSpec: charts.NoneRenderSpec()
                  ),
//            barRendererDecorator: charts.BarLabelDecorator(),
             // vertical: false,
          ),
        ),
      ],
    );
  }

   CreateData() async{
    final dio = Dio();
    final client = RestClient(dio);
    client.getTasks().then((it) {
      setState(() {
        switch(widget.name){
          case city_name.Agra:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.Agra.active));
            desktopSalesData.add(BarChartStructure(district: "Cnf",cause: it.myState.data.Agra.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.Agra.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.Agra.deceased));
            break;
          case city_name.Aligarh:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.aligarh.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.aligarh.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.aligarh.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.aligarh.deceased));
            break;
          case city_name.Ambedkar_Nagar:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.amber_nagar.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.amber_nagar.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.amber_nagar.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.amber_nagar.deceased));
            break;
          case city_name.Amethi:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.amethi.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.amethi.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.amethi.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.amethi.deceased));
            break;
          case city_name.Amroha:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.amroha.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.amroha.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.amroha.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.amroha.deceased));
            break;
          case city_name.Auriaya:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.auriya.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.auriya.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.auriya.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.auriya.deceased));
            break;
          case city_name.Ayodhya:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.ayodhya.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.ayodhya.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.ayodhya.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.ayodhya.deceased));
            break;
          case city_name.Azamgarh:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.azamgarh.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.azamgarh.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.azamgarh.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.azamgarh.deceased));
            break;
          case city_name.Bagpath:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.bagpath.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.bagpath.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.bagpath.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.bagpath.deceased));
            break;
          case city_name.Balrampur:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.balrampur.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.balrampur.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.balrampur.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.balrampur.deceased));
            break;
          case city_name.Banda:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.banda.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.banda.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.banda.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.banda.deceased));
            break;
          case city_name.Barabanki:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.barabanki.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.barabanki.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.barabanki.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.barabanki.deceased));
            break;
          case city_name.Bareilly:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.bareilly.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.bareilly.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.bareilly.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.bareilly.deceased));
            break;
          case city_name.Basti:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.basti.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.basti.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.basti.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.basti.deceased));
            break;
          case city_name.Bhadohi:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.bhadohi.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.bhadohi.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.bhadohi.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.bhadohi.deceased));
            break;
          case city_name.Bijnor:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.bijnor.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.bijnor.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.bijnor.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.bijnor.deceased));
            break;
          case city_name.Budaun:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.budaun.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.budaun.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.budaun.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.budaun.deceased));
            break;
          case city_name.Bulandshahr:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.bulandsahr.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.bulandsahr.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.bulandsahr.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.bulandsahr.deceased));
            break;
          case city_name.Chandauli:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.chandauli.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.chandauli.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.chandauli.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.chandauli.deceased));
            break;
          case city_name.Chitrakoot:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.chitrakoot.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.chitrakoot.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.chitrakoot.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.chitrakoot.deceased));
            break;
          case city_name.Etawah:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.etawah.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.etawah.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.etawah.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.etawah.deceased));
            break;
          case city_name.Farrukhabad:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.farrukhabad.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.farrukhabad.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.farrukhabad.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.farrukhabad.deceased));
            break;
          case city_name.Firozabad:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.firozabad.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.firozabad.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.firozabad.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.firozabad.deceased));
            break;
          case city_name.Ghaziabad:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.ghaziabad.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.ghaziabad.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.ghaziabad.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.ghaziabad.deceased));
            break;
          case city_name.Gonda:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.gonda.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.gonda.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.gonda.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.gonda.deceased));
            break;
          case city_name.Hapur:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.hapur.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.hapur.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.hapur.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.hapur.deceased));
            break;
          case city_name.Kanpur_Nagar:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.kanpur.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.kanpur.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.kanpur.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.kanpur.deceased));
            break;
          case city_name.Lucknow:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.lucknow.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.lucknow.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.lucknow.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.lucknow.deceased));
            break;
          case city_name.Meerut:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.meerut.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.meerut.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.meerut.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.meerut.deceased));
            break;
          case city_name.Moradabad:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.moradabad.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.moradabad.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.moradabad.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.moradabad.deceased));
            break;
          case city_name.Pratapgarh:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.pratapgarh.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.pratapgarh.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.pratapgarh.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.pratapgarh.deceased));
            break;
          case city_name.Prayagraj:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.prayagraj.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.prayagraj.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.prayagraj.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.prayagraj.deceased));
            break;
          case city_name.Rae_Bareli:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.raeBareli.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.raeBareli.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.raeBareli.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.raeBareli.deceased));
            break;
          case city_name.Rampur:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.rampur.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.rampur.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.rampur.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.rampur.deceased));
            break;
          case city_name.Sitapur:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.sitapur.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.sitapur.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.sitapur.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.sitapur.deceased));
            break;
          case city_name.Unnao:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.unnao.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.unnao.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.unnao.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.unnao.deceased));
            break;
          case city_name.Varanasi:
            desktopSalesData.add(BarChartStructure(district:"Act",cause: it.myState.data.varanasi.active));
            desktopSalesData.add(BarChartStructure(district: "Conf",cause: it.myState.data.varanasi.confirmed));
            desktopSalesData.add(BarChartStructure(district: "Rec",cause: it.myState.data.varanasi.recovered));
            desktopSalesData.add(BarChartStructure(district: "Dec",cause: it.myState.data.varanasi.deceased));
            break;
        }

      });
    }).catchError((onError){
      print(onError.toString());
    });
  }

}

enum city_name{
  Agra,
  Aligarh,
  Ambedkar_Nagar,
  Amethi,
  Amroha,
  Auriaya,
  Ayodhya,
  Azamgarh,
  Bagpath,
  Balrampur,
  Banda,
  Barabanki,
  Bareilly,
  Basti,
  Bhadohi,
  Bijnor,
  Budaun,
  Bulandshahr,
  Chandauli,
  Chitrakoot,
  Etawah,
  Farrukhabad,
  Firozabad,
  Ghaziabad,
  Gonda,
  Hapur,
  Kanpur_Nagar,
  Lucknow,
  Meerut,
  Moradabad,
  Pratapgarh,
  Prayagraj,
  Rae_Bareli,
  Rampur,
  Sitapur,
  Unnao,
  Varanasi,
}