import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Bar_Chart.dart';

//void main() {
//  runApp(SimpleBarChart());

//}

//class SimpleBarChart extends StatefulWidget {
//  @override
//  SimpleBarChartState createState() => SimpleBarChartState();
//}
//
//class SimpleBarChartState extends State<SimpleBarChart> {
//  final mylist = list_seriesState();
//
//  PageController pgtrl = PageController(viewportFraction: .8);
//
//  var currentPage = 0;
//  bool flag = false;
//  List<String> graphs = new List();
//  List<int> graphs1 = new List();
//  @override
//  void initState() {
//    pgtrl.addListener(() {
//      var next  =  pgtrl.page.round();
//      setState(() {
//        if(currentPage != next){
//          currentPage = next;
//          flag = !flag;
//          print(currentPage);
//        }
//      });
//    });
//    setState(() {
//      graphs = [ "Agra", "Aligarh", "Ambedkar Nagar", "Amethi", "Amroha",
//        'Auriaya',
//        'Ayodhya',
//        'Azamgarh',
//        'Bagpath',
//        'Balrampur',
//        'Banda',
//        'Barabanki',
//        'Bareilly',
//        'Basti',
//        'Bhadohi',
//        'Bijnor',
//        'Budaun',
//        'Bulandshahr',
//        'Chandauli',
//        'Chitrakoot',
//        'Etawah',
//        'Farrukhabad',
//        'Firozabad',
//        'Ghaziabad',
//        'Gonda',
//        'Hapur',
//        'Kanpur_Nagar',
//        'Lucknow',
//        'Meerut',
//        'Moradabad',
//        'Pratapgarh',
//        'Prayagraj',
//        'Rae_Bareli',
//        'Rampur',
//        'Sitapur',
//        'Unnao',
//        'Varanasi',
//      ];
//      graphs1 = [
//        0,
//        1,
//        2,
//        3,
//        4,
//        5,
//        6,
//        7,
//        8,
//        9,
//        10,
//        11,
//        12,
//        13,
//        14,
//        15,
//        16,
//        17,
//        18,
//        19,
//        20,
//        21,
//        22,
//        23,
//        24,
//        25,
//        26,
//        27,
//        28,
//        29,
//        30,
//        31,
//        32,
//        33,
//        34,
//        35,
//        36
//      ];
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return MaterialApp(
//      debugShowCheckedModeBanner: false,
//      home: Scaffold(
//        body: MyPageView()
//      )
//    );
//  }
//
//  Widget MyPageView(){
//    return PageView.builder(
//      itemCount: graphs.length,
//      itemBuilder: (BuildContext context, int index) {
//      return Chart_Decorator(graphs[index], graphs1[index], index == currentPage);
//      },
//    );
//  }
//
//  Widget Chart_Decorator(String city, int val,bool flag){
//    return AnimatedContainer(
//      duration: Duration(milliseconds: 500),
//      curve: Curves.easeIn,
//      margin: EdgeInsets.only(top: flag?100 :200,bottom: 30,left: 5, right: 15),
//      child: Card(
//        color: Color(0xffDAEDFE),
//        shape: RoundedRectangleBorder(
//          borderRadius: BorderRadius.only(
//              topLeft: Radius.circular(20),
//              topRight: Radius.circular(20),
//              bottomLeft:Radius.circular(20),
//              bottomRight: Radius.circular(20)),
//        ),
//        child: Column(
//          crossAxisAlignment: CrossAxisAlignment.start,
//          children: <Widget>[
//            Container(
//              margin: EdgeInsets.only(left: 16,top: 16),
//                child: Create_card("Statistics")),
//            Expanded(
//              child: Container(
//                  alignment: Alignment.center,
//                  margin: EdgeInsets.only(top: 40,bottom: 40,left: 15, right: 15),
//                  color: Color(0xffEFF8FF),
//                  child: list_series(cityname: city,name: city_name.values[val])
//              ),
//            ),
//          ],
//        ),
//      ) ,
//      decoration: new BoxDecoration(boxShadow: [
//        new BoxShadow(
//          color: Colors.black26,
//          blurRadius: 8.0,
//        ),
//      ]),
//    );
//  }
//
//  Widget Create_card(String text) {
//    return Container(
//      width: 100,
//      //margin: EdgeInsets.only(left: 16, right: 16),
//      padding: EdgeInsets.only(bottom: 5, top: 5),
//      decoration: BoxDecoration(
//        gradient: LinearGradient(
//          begin: Alignment.bottomLeft,
//          end: Alignment.topRight,
//          colors: [
//            Colors.blue,
//            Colors.lightBlueAccent,
//          ],
//        ),
//        borderRadius: BorderRadius.circular(32),
//      ),
//      child: Center(
//          child: Text(
//            text,
//            style: TextStyle(fontSize: 16, color: Colors.white),
//          )),
//    );
//  }
//}
//
///// Sample ordinal data type.
