import 'package:charts_flutter/flutter.dart' as charts;
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xdtester/Api/Date_wise_data.dart';
import 'Bar_Chart.dart';
import 'Line_Chart.dart';

//void main() {
//  runApp(SimpleBarChart1());
//}

class SimpleBarChart1 extends StatefulWidget {
  @override
  _SimpleBarChartState1 createState() => _SimpleBarChartState1();
}

class _SimpleBarChartState1 extends State<SimpleBarChart1> {
  final mylist = list_seriesState();

  PageController pgtrl = PageController(viewportFraction: .8,initialPage: 0);
  List<double> my = [0,1,2];
  double height = 100;
  var currentPage = 0;
  bool flag = false;
  var ff = 1;

  List<String> graphs = new List();
  List<int> graphs1 = new List();
  @override
  void initState() {
    pgtrl.addListener(() {
     var next  =  pgtrl.page.round();
      setState(() {
        if(currentPage != next){
          currentPage = next;
          flag = !flag;
          print(currentPage);
        }
      });
    });
    setState(() {
      graphs = [ "Agra","Aligarh","Ambedkar Nagar","Amethi","Amroha",
        'Auriaya',
        'Ayodhya',
        'Azamgarh',
        'Bagpath',
        'Balrampur',
        'Banda',
        'Barabanki',
        'Bareilly',
        'Basti',
        'Bhadohi',
        'Bijnor',
        'Budaun',
        'Bulandshahr',
        'Chandauli',
        'Chitrakoot',
        'Etawah',
        'Farrukhabad',
        'Firozabad',
        'Ghaziabad',
        'Gonda',
        'Hapur',
        'Kanpur_Nagar',
        'Lucknow',
        'Meerut',
        'Moradabad',
        'Pratapgarh',
        'Prayagraj',
        'Rae_Bareli',
        'Rampur',
        'Sitapur',
        'Unnao',
        'Varanasi',];
      graphs1 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,
      32,33,34,35,36];
//        Chart_Decorator("Agra", 0),
//            Chart_Decorator("Aligarh", 1),
//            Chart_Decorator("Ambedkar Nagar", 2),
//            Chart_Decorator("Amethi", 3),
//            Chart_Decorator("Amroha", 4),
//            Chart_Decorator("Auriaya", 5),
//            Chart_Decorator("Ayodhya", 6),
//            Chart_Decorator("Azamgarh", 7),
//            Chart_Decorator("Bagpath", 8),
//            Chart_Decorator("Balrampur", 9),
//            Chart_Decorator("Banda", 10),
//            Chart_Decorator("Barabanki", 11),
//            Chart_Decorator("Bareilly", 12),
//            Chart_Decorator("Basti", 13),
//            Chart_Decorator("Bhadohi", 14),
//            Chart_Decorator("Bijnor", 15),
//            Chart_Decorator("Budaun", 16),
//            Chart_Decorator("Bulandshahr", 17),
//            Chart_Decorator("Chandauli", 18),
//            Chart_Decorator("Chitrakoot", 19),
//            Chart_Decorator("Etawah", 20),
//            Chart_Decorator("Farrrukhabad", 21),
//            Chart_Decorator("Firozabad", 22),
//            Chart_Decorator("Ghaziabad", 23),
//            Chart_Decorator("Gonda", 24),
//            Chart_Decorator("Hapur", 25),
//            Chart_Decorator("Kanpur", 26),
//            Chart_Decorator("Lucknow", 27),
//            Chart_Decorator("Meerut", 28),
//            Chart_Decorator("Moradabad", 29),
//            Chart_Decorator("Pratapgarh", 30),
//            Chart_Decorator("Prayagraj", 31),
//            Chart_Decorator("Rae Barelli", 32),
//            Chart_Decorator("Rampur", 33),
//            Chart_Decorator("Sitapur", 34),
//            Chart_Decorator("Unnao", 35),
//            Chart_Decorator("Varanasi", 36)

    });
    createdata();
    print(graphs.length);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
        color: Colors.white,
        home: Scaffold(
          body: SafeArea(
              child: PageView.builder(
                itemCount: graphs.length,
                onPageChanged: (val){
                  setState(() {
                    ff = val;
                    print("ff"+ ff.toString());
                  });
                  //print(val);
                },
                controller: pgtrl,
                itemBuilder: (BuildContext context, int index) {
                  return Chart_Decorator(graphs[index], graphs1[index], index == currentPage);
                },
              )
          ),
        )
    );
  }

  Widget Chart_Decorator(String city, int val,bool flag){
    return AnimatedContainer(
      duration: Duration(milliseconds: 100),
      curve: Curves.bounceIn,
      margin: EdgeInsets.only(top: flag ? 100 :200 ,bottom: 120,left: 15, right: 15),
      child: Card(
        color: Color(0xffDAEDFE),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
              bottomLeft:Radius.circular(20),
              bottomRight: Radius.circular(20)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(left: 16,top: 16),
                child: Create_card("Statistics")),
            Expanded(
              child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(top: 40,bottom: 40,left: 15, right: 15),
                  color: Color(0xffEFF8FF),
                  child: Text(city)
              ),
            ),
          ],
        ),
      ) ,
      decoration: new BoxDecoration(boxShadow: [
        new BoxShadow(
          color: Colors.black26,
          blurRadius: 8.0,
        ),
      ]),
    );
  }

  Widget Create_card(String text) {
    return Container(
      width: 100,
      padding: EdgeInsets.only(bottom: 5, top: 5),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            Colors.blue,
            Colors.lightBlueAccent,
          ],
        ),
        borderRadius: BorderRadius.circular(32),
      ),
      child: Center(
          child: Text(
            text,
            style: TextStyle(fontSize: 16, color: Colors.white),
          )),
    );
  }

  void createdata() async{
    final dio = Dio();
    final client = RestClient(dio);
    client.getDateWiseData().then((it) {
      print(it.name.city.Agra[0].date);
    }).catchError((onError){
      print(onError.toString());
    });
  }
}

/// Sample ordinal data type.
