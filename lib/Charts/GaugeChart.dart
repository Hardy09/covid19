import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:xdtester/Api/Fetched_Data.dart';
import 'package:xdtester/Charts/Charts_Structure_body.dart';
import '../Tab_Bar.dart';

//void main() {
//  runApp(Guage_Chart());
//}

class Guage_Chart extends StatefulWidget{
//  String cityname;
//  //List<LineChartStructure> data = new List();
//  city_name_date name;
//  String value = "0";
//  List<LineChartStructure> mydesktopSales = new List<LineChartStructure>();
//  Line_Chart({this.cityname,this.name,this.mydesktopSales,this.value});
  @override
  Guage_ChartState createState() => Guage_ChartState();
}

class Guage_ChartState extends State<Guage_Chart> {
  List<int> confirm = new List();
  List<int> deceased = new List();
  List<int> recovered = new List();
  List<int> active = new List();
  int confCounter = 0;
  int decCounter = 0;
  int recCounter = 0;
  int actCounter = 0;



  @override
  void initState() {
    CreateData();
  }


  @override
  Widget build(BuildContext context) {

    final data = [
      new PieChartStructure('Active', actCounter ),
      new PieChartStructure('Confirm', confCounter),
      new PieChartStructure('Recovered', recCounter),
      new PieChartStructure('Deceased', decCounter),
    ];

    List<charts.Series<PieChartStructure, String>> seriesList =[ charts.Series(
        id: 'Segments',
        domainFn: (PieChartStructure segment, _) => segment.segment,
        measureFn: (PieChartStructure segment, _) => segment.size,
        data: data,
        //colorFn: (_, __) => charts.ColorUtil.fromDartColor(Colors.lightBlueAccent),
//          fillColorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        // labelAccessorFn: (datum, index) => "hello",
      )
      ];


    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
//        backgroundColor: Color(0xffB6D2EB),
        body: Container(
          margin: EdgeInsets.only(top: 25),
          height: 170,
          width: 500,
          child: Stack(
            overflow: Overflow.clip,
            children: <Widget>[
              Container(
                //margin: EdgeInsets.only(top: 30),
                width: 180,
                height: 200,
                child: new charts.PieChart(
                  seriesList,
                  animate: true,
                  defaultRenderer: new charts.ArcRendererConfig(
                      arcWidth: 30, startAngle: 4 / 5 * 3.14, arcLength: 7 / 5 * 3.14),
                ),
              ),
              Positioned(
                left: 70,
                top: 70,
                child: Text("UP Data",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold)),
              ),
              Positioned(
                left: 35,
                top: 90,
                child: Text("Act",style: TextStyle(color: Colors.white,fontSize: 8),),
              ),
              Positioned(
                left: 70,
                top: 35,
                child: Text("Cnf",style: TextStyle(color: Colors.white,fontSize: 8),),
              ),
              Positioned(
                left: 130,
                top: 70,
                child: Text("Rec",style: TextStyle(color: Colors.white,fontSize: 8),),
              ),
              Positioned(
                left: 180,
                child: DataContain(0xffFFFFFF,0xffFA9292,"Active Cases",actCounter.toString(),"assets/Images/line1.png"),
              ),
              Positioned(
                left: 220,
                top: 60,
                child:DataContain(0xffFFFFFF,0xffA0D897,"Recovered",recCounter.toString(),"assets/Images/line2.png"),
              ),
              Positioned(
                left: 180,
                top: 120,
                child:DataContain(0xffFFFFFF,0xffAC8FFA,"Confirmed",confCounter.toString(),"assets/Images/line3.png"),
              )
            ],
          ),
        ),
      ),
    );
  }


  Widget DataContain(int color1,int color2,String text1,String text2,String link){
    return Container(
      height: 50,
      width: 120,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: <Color>[
              Color(color1),
              Color(color2)
            ]
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        margin: EdgeInsets.only(top: 11),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(text1,style: TextStyle(color: Colors.black,fontSize: 10,fontWeight: FontWeight.bold)),
                Text(text2,style: TextStyle(color: Colors.black,fontSize: 10,fontWeight: FontWeight.bold))
              ],
            ),
            Image.asset(link)
          ],
        ),
      ),
    );
  }


  CreateData() async{
    final dio = Dio();
    final client = RestClient(dio);
    client.getTasks().then((it) {
            active.add(it.myState.data.Agra.active);
            confirm.add(it.myState.data.Agra.confirmed);
            recovered.add(it.myState.data.Agra.recovered);
            deceased.add(it.myState.data.Agra.deceased);

            active.add(it.myState.data.aligarh.active);
            confirm.add(it.myState.data.aligarh.confirmed);
            recovered.add(it.myState.data.aligarh.recovered);
            deceased.add(it.myState.data.aligarh.deceased);

            active.add(it.myState.data.amber_nagar.active);
            confirm.add(it.myState.data.amber_nagar.confirmed);
            recovered.add(it.myState.data.amber_nagar.recovered);
            deceased.add(it.myState.data.amber_nagar.deceased);

            active.add(it.myState.data.amethi.active);
            confirm.add(it.myState.data.amethi.confirmed);
            recovered.add(it.myState.data.amethi.recovered);
            deceased.add(it.myState.data.amethi.deceased);

            active.add(it.myState.data.amroha.active);
            confirm.add(it.myState.data.amroha.confirmed);
            recovered.add(it.myState.data.amroha.recovered);
            deceased.add(it.myState.data.amroha.deceased);

            active.add(it.myState.data.auriya.active);
            confirm.add(it.myState.data.auriya.confirmed);
            recovered.add(it.myState.data.auriya.recovered);
            deceased.add(it.myState.data.auriya.deceased);

            active.add(it.myState.data.ayodhya.active);
            confirm.add(it.myState.data.ayodhya.confirmed);
            recovered.add(it.myState.data.ayodhya.recovered);
            deceased.add(it.myState.data.ayodhya.deceased);

            active.add(it.myState.data.azamgarh.active);
            confirm.add(it.myState.data.azamgarh.confirmed);
            recovered.add(it.myState.data.azamgarh.recovered);
            deceased.add(it.myState.data.azamgarh.deceased);

            active.add(it.myState.data.bagpath.active);
            confirm.add(it.myState.data.bagpath.confirmed);
            recovered.add(it.myState.data.bagpath.recovered);
            deceased.add(it.myState.data.bagpath.deceased);

            active.add(it.myState.data.balrampur.active);
            confirm.add(it.myState.data.balrampur.confirmed);
            recovered.add(it.myState.data.balrampur.recovered);
            deceased.add(it.myState.data.balrampur.deceased);

            active.add(it.myState.data.banda.active);
            confirm.add(it.myState.data.banda.confirmed);
            recovered.add(it.myState.data.banda.recovered);
            deceased.add(it.myState.data.banda.deceased);

            active.add(it.myState.data.barabanki.active);
            confirm.add(it.myState.data.barabanki.confirmed);
            recovered.add(it.myState.data.barabanki.recovered);
            deceased.add(it.myState.data.barabanki.deceased);

            active.add(it.myState.data.bareilly.active);
            confirm.add(it.myState.data.bareilly.confirmed);
            recovered.add(it.myState.data.bareilly.recovered);
            deceased.add(it.myState.data.bareilly.deceased);

            active.add(it.myState.data.basti.active);
            confirm.add(it.myState.data.basti.confirmed);
            recovered.add(it.myState.data.basti.recovered);
            deceased.add(it.myState.data.basti.deceased);

            active.add(it.myState.data.bhadohi.active);
            confirm.add(it.myState.data.bhadohi.confirmed);
            recovered.add(it.myState.data.bhadohi.recovered);
            deceased.add(it.myState.data.bhadohi.deceased);

            active.add(it.myState.data.bijnor.active);
            confirm.add(it.myState.data.bijnor.confirmed);
            recovered.add(it.myState.data.bijnor.recovered);
            deceased.add(it.myState.data.bijnor.deceased);

            active.add(it.myState.data.budaun.active);
            confirm.add(it.myState.data.budaun.confirmed);
            recovered.add(it.myState.data.budaun.recovered);
            deceased.add(it.myState.data.budaun.deceased);

            active.add(it.myState.data.bulandsahr.active);
            confirm.add(it.myState.data.bulandsahr.confirmed);
            recovered.add(it.myState.data.bulandsahr.recovered);
            deceased.add(it.myState.data.bulandsahr.deceased);

            active.add(it.myState.data.chandauli.active);
            confirm.add(it.myState.data.chandauli.confirmed);
            recovered.add(it.myState.data.chandauli.recovered);
            deceased.add(it.myState.data.chandauli.deceased);

            active.add(it.myState.data.chitrakoot.active);
            confirm.add(it.myState.data.chitrakoot.confirmed);
            recovered.add(it.myState.data.chitrakoot.recovered);
            deceased.add(it.myState.data.chitrakoot.deceased);

            active.add(it.myState.data.etawah.active);
            confirm.add(it.myState.data.etawah.confirmed);
            recovered.add(it.myState.data.etawah.recovered);
            deceased.add(it.myState.data.etawah.deceased);

            active.add(it.myState.data.farrukhabad.active);
            confirm.add(it.myState.data.farrukhabad.confirmed);
            recovered.add(it.myState.data.farrukhabad.recovered);
            deceased.add(it.myState.data.farrukhabad.deceased);

            active.add(it.myState.data.firozabad.active);
            confirm.add(it.myState.data.firozabad.confirmed);
            recovered.add(it.myState.data.firozabad.recovered);
            deceased.add(it.myState.data.firozabad.deceased);

            active.add(it.myState.data.ghaziabad.active);
            confirm.add(it.myState.data.ghaziabad.confirmed);
            recovered.add(it.myState.data.ghaziabad.recovered);
            deceased.add(it.myState.data.ghaziabad.deceased);

            active.add(it.myState.data.gonda.active);
            confirm.add(it.myState.data.gonda.confirmed);
            recovered.add(it.myState.data.gonda.recovered);
            deceased.add(it.myState.data.gonda.deceased);

            active.add(it.myState.data.lucknow.active);
            confirm.add(it.myState.data.lucknow.confirmed);
            recovered.add(it.myState.data.lucknow.recovered);
            deceased.add(it.myState.data.lucknow.deceased);

            active.add(it.myState.data.meerut.active);
            confirm.add(it.myState.data.meerut.confirmed);
            recovered.add(it.myState.data.meerut.recovered);
            deceased.add(it.myState.data.meerut.deceased);

            active.add(it.myState.data.moradabad.active);
            confirm.add(it.myState.data.moradabad.confirmed);
            recovered.add(it.myState.data.moradabad.recovered);
            deceased.add(it.myState.data.moradabad.deceased);

            active.add(it.myState.data.pratapgarh.active);
            confirm.add(it.myState.data.pratapgarh.confirmed);
            recovered.add(it.myState.data.pratapgarh.recovered);
            deceased.add(it.myState.data.pratapgarh.deceased);

            active.add(it.myState.data.prayagraj.active);
            confirm.add(it.myState.data.prayagraj.confirmed);
            recovered.add(it.myState.data.prayagraj.recovered);
            deceased.add(it.myState.data.prayagraj.deceased);

            active.add(it.myState.data.raeBareli.active);
            confirm.add(it.myState.data.raeBareli.confirmed);
            recovered.add(it.myState.data.raeBareli.recovered);
            deceased.add(it.myState.data.raeBareli.deceased);

            active.add(it.myState.data.rampur.active);
            confirm.add(it.myState.data.rampur.confirmed);
            recovered.add(it.myState.data.rampur.recovered);
            deceased.add(it.myState.data.rampur.deceased);

            active.add(it.myState.data.sitapur.active);
            confirm.add(it.myState.data.sitapur.confirmed);
            recovered.add(it.myState.data.sitapur.recovered);
            deceased.add(it.myState.data.sitapur.deceased);

            active.add(it.myState.data.unnao.active);
            confirm.add(it.myState.data.unnao.confirmed);
            recovered.add(it.myState.data.unnao.recovered);
            deceased.add(it.myState.data.unnao.deceased);

            active.add(it.myState.data.varanasi.active);
            confirm.add(it.myState.data.varanasi.confirmed);
            recovered.add(it.myState.data.varanasi.recovered);
            deceased.add(it.myState.data.varanasi.deceased);
            print("data added");
            print(confirm.length);
            setState(() {
              int count = 0;
              confirm.forEach((element) {
                confCounter+=element;
              });
              deceased.forEach((element) {
                decCounter+=element;
              });
              recovered.forEach((element) {
                recCounter+=element;
              });
              active.forEach((element) {
                actCounter+=element;
              });
            });
    }).catchError((onError){
      print(onError.toString());
    });
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }

}
