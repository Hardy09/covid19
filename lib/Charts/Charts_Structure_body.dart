class BarChartStructure {
  String district;
  int cause;

  BarChartStructure({this.district, this.cause});
}

class LineChartStructure {
  DateTime date;
  int cause;

  LineChartStructure({this.date, this.cause});
}

class PieChartStructure {
  final String segment;
  final int size;

  PieChartStructure(this.segment, this.size);
}

