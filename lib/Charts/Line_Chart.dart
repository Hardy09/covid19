import 'package:flutter/cupertino.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:xdtester/Charts/Charts_Structure_body.dart';

//void main() {
//  runApp(Line_Chart());
//}

class Line_Chart extends StatefulWidget{
  String cityname;
  //List<LineChartStructure> data = new List();
  city_name_date name;
  String value = "0";
  List<LineChartStructure> mydesktopSales = new List<LineChartStructure>();
  Line_Chart({this.cityname,this.name,this.mydesktopSales,this.value});
  @override
  Line_ChartState createState() => Line_ChartState();
}

 class Line_ChartState extends State<Line_Chart> {
  List<LineChartStructure> desktopSales = new List<LineChartStructure>();
  List<int> confirm = new List();

  @override
  void initState() {
    //CreateData();
  }

  Widget build(BuildContext context) {
    List<charts.Series<LineChartStructure, DateTime>> series = new List<charts.Series<LineChartStructure, DateTime>>();
    // TODO: implement build
    switch(widget.name) {
      case city_name_date.Agra:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Aligarh:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Ambedkar_Nagar:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Amethi:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Amroha:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Auriaya:
        series.add(
            charts.Series(
              data:widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Ayodhya:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Azamgarh:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Bagpath:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Balrampur:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Banda:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Barabanki:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Bareilly:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Basti:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Bhadohi:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Etawah:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;

      case city_name_date.Pratapgarh:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Prayagraj:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Rae_Bareli:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Varanasi:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Lucknow:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Gaziabad:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Hardoi:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Jhansi:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Meerut:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Jaunpur:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
      case city_name_date.Mathura:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;

      case city_name_date.Kanpur_Nagar:
        series.add(
            charts.Series(
              data: widget.mydesktopSales,
              domainFn: (LineChartStructure sales, _) => sales.date,
              measureFn: (LineChartStructure sales, _) => sales.cause,
              id: 'MY DATA',
              colorFn: (_, __) => charts.MaterialPalette.white,
            )
        );
        break;
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 4),
            child: Text("Confirmed Cases: "+widget.value,style: TextStyle(color: Colors.white),)),
        Expanded(
          child: charts.TimeSeriesChart(
            series,
            animate: true,
            primaryMeasureAxis: new charts.NumericAxisSpec(
              showAxisLine: false,
              renderSpec: new charts.NoneRenderSpec(),
            ),
            domainAxis: new charts.DateTimeAxisSpec(
              showAxisLine: false,
              renderSpec: new charts.NoneRenderSpec(),
            ),
          ),
        ),
      ],
    );
  }

 }

enum city_name_date{
  Agra,
  Aligarh,
  Ambedkar_Nagar,
  Amethi,
  Amroha,
  Auriaya,
  Ayodhya,
  Azamgarh,
  Bagpath,
  Balrampur,
  Banda,
  Barabanki,
  Bareilly,
  Basti,
  Bhadohi,
  Etawah,
  Kanpur_Nagar,
  Pratapgarh,
  Prayagraj,
  Rae_Bareli,
  Varanasi,
  Lucknow,
  Gaziabad,
  Hardoi,
  Jhansi,
  Meerut,
  Jaunpur,
  Mathura
}