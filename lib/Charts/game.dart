import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xdtester/Api/Date_wise_data.dart';
import 'package:xdtester/Charts/Charts.dart';
import 'Bar_Chart.dart';
import 'Line_Chart.dart';
import 'package:xdtester/Charts/Charts_Structure_body.dart';

//void main() {
//  runApp(SampleBarChart());
//}

class SampleBarChart extends StatefulWidget {
  @override
  SimpleLineChartState createState() => SimpleLineChartState();
}

class SimpleLineChartState extends State<SampleBarChart> {
  final mylist = list_seriesState();
//  final barchart = SimpleBarChartState();
  List<LineChartStructure> desktopSales = new List<LineChartStructure>();
  List<int> confirm = new List();


  PageController pgtrl = PageController(viewportFraction: .8);

  var currentPage = 0;
  bool flag = false;
  List<String> graphs = new List();
  List<int> graphs1 = new List();

  @override
  void initState() {
    CreateData();
    pgtrl.addListener(() {
      var next  =  pgtrl.page.round();
      setState(() {
        if(currentPage != next){
          currentPage = next;
          flag = !flag;
          print(currentPage);
        }
      });
    });
    setState(() {
      graphs = [ "Agra", "Aligarh", "Ambedkar Nagar", "Amethi", "Amroha",
        'Auriaya',
        'Ayodhya',
        'Azamgarh',
        'Bagpath',
        'Balrampur',
        'Banda',
        'Barabanki',
        'Bareilly',
        'Basti',
        'Bhadohi',
        'Bijnor',
        'Budaun',
        'Bulandshahr',
        'Chandauli',
        'Chitrakoot',
        'Etawah',
        'Farrukhabad',
        'Firozabad',
        'Ghaziabad',
        'Gonda',
        'Hapur',
        'Kanpur_Nagar',
        'Lucknow',
        'Meerut',
        'Moradabad',
        'Pratapgarh',
        'Prayagraj',
        'Rae_Bareli',
        'Rampur',
        'Sitapur',
        'Unnao',
        'Varanasi',
      ];
      graphs1 = [
        0,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
        31,
        32,
        33,
        34,
        35,
        36
      ];
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Color(0xffB6D2EB),
          body: Column(
            children: <Widget>[
              AnalysisPart()
              //SimpleBarChart()
            ],
          ),
        )
    );
  }

  Widget AnalysisPart(){
    return Expanded(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
            topRight: Radius.circular(40),
            bottomLeft: Radius.circular(40),
            bottomRight: Radius.circular(40)),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Results Of Uttar Pradesh:", style: TextStyle(color: Colors.black45,
                      fontWeight: FontWeight.bold,fontSize: 20)),
                ],
              ),
              Container(
                height: 135,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  children: <Widget>[
                    Chart_Decorator("Agra",0,desktopSales.length >0? desktopSales.sublist(0,6)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[0].toString() : "0"),
                    Chart_Decorator("Aligarh", 1,desktopSales.length > 0 ? desktopSales.sublist(6, 12)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[1].toString() : "0"),
                    Chart_Decorator("Ambedkar Nagar", 2,desktopSales.length >0? desktopSales.sublist(12,18)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[2].toString() : "0"),
                    Chart_Decorator("Amethi", 3, desktopSales.length >0? desktopSales.sublist(18,24)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[3].toString() : "0"),
                    Chart_Decorator("Amroha", 4, desktopSales.length >0? desktopSales.sublist(18,24)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[4].toString() : "0"),
                    Chart_Decorator("Auriaya", 5,desktopSales.length >0? desktopSales.sublist(30,36)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[5].toString() : "0"),
                    Chart_Decorator("Ayodhya", 6,desktopSales.length >0? desktopSales.sublist(36,42)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[6].toString() : "0"),
                    Chart_Decorator("Azamgarh", 7,desktopSales.length >0? desktopSales.sublist(42,48)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[7].toString() : "0"),
                    Chart_Decorator("Bagpath", 8,desktopSales.length >0? desktopSales.sublist(48,54)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[8].toString() : "0"),
                    Chart_Decorator("Balrampur", 9,desktopSales.length >0? desktopSales.sublist(54,60)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[9].toString() : "0"),
                    Chart_Decorator("Banda", 10,desktopSales.length >0? desktopSales.sublist(60,66)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[10].toString() : "0"),
                    Chart_Decorator("Barabanki", 11,desktopSales.length >0? desktopSales.sublist(66,72)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[11].toString() : "0"),
                    Chart_Decorator("Bareilly", 12,desktopSales.length >0? desktopSales.sublist(72,78)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[12].toString() : "0"),
                    Chart_Decorator("Basti", 13, desktopSales.length >0? desktopSales.sublist(78,84)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[13].toString() : "0"),
                    Chart_Decorator("Bhadohi", 14,desktopSales.length >0? desktopSales.sublist(84,90)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[14].toString() : "0"),
                    Chart_Decorator("Etawah", 15, desktopSales.length >0? desktopSales.sublist(90,96)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[15].toString() : "0"),
                    Chart_Decorator("Kanpur", 16,desktopSales.length >0? desktopSales.sublist(102,108)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[16].toString() : "0"),
                    Chart_Decorator("Pratapgarh", 17, desktopSales.length >0? desktopSales.sublist(96,102)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[17].toString() : "0"),
                    Chart_Decorator("Prayagraj", 18,desktopSales.length >0? desktopSales.sublist(108,114)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[18].toString() : "0"),
                    Chart_Decorator("Rae Bareli", 19,desktopSales.length >0? desktopSales.sublist(114,120)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[18].toString() : "0"),
                    Chart_Decorator("Varanasi", 20, desktopSales.length >0? desktopSales.sublist(120,126)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[19].toString() : "0"),
                    Chart_Decorator("Lucknow", 21,desktopSales.length >0? desktopSales.sublist(126,132)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[20].toString() : "0"),
                    Chart_Decorator("Gaziabad", 22, desktopSales.length >0? desktopSales.sublist(132,138)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[21].toString() : "0"),
                    Chart_Decorator("Hardoi", 23,desktopSales.length >0? desktopSales.sublist(138,144)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[22].toString() : "0"),
                    Chart_Decorator("Jhansi", 24,desktopSales.length >0? desktopSales.sublist(144,150)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[23].toString() : "0"),
                    Chart_Decorator("Meerut", 25, desktopSales.length >0? desktopSales.sublist(150,156)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[24].toString() : "0"),
                    Chart_Decorator("Jaunpur", 26,desktopSales.length >0? desktopSales.sublist(156,162)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[25].toString() : "0"),
                    Chart_Decorator("Mathura", 27, desktopSales.length >0? desktopSales.sublist(162,168)
                        : desktopSales.sublist(0),confirm.length > 0 ? confirm[26].toString() : "0"),
                  ],
                ),
              ),
              Container(
                  height: 370,
                  child: MyPageView())
            ],
          ),
        ),
      ),
    );
  }

  CreateData(){
    final dio = Dio();
    final client = RestClient(dio);
    client.getDateWiseData().then((it) {
      ListReturn(it);
    }).catchError((onError){
      print(onError.toString());
    });
  }

  Widget MyPageView(){
    return PageView.builder(
      controller: pgtrl,
      itemCount: graphs.length,
      itemBuilder: (BuildContext context, int index) {
        return Chart_Decorator_new(graphs[index], graphs1[index], index == currentPage);
      },
    );
  }

  List ListReturn(DistrictData it){
    List<LineChartStructure> getlist = new List();
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Agra[0].date),
        cause: it.name.city.Agra[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Agra[10].date),
        cause: it.name.city.Agra[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Agra[20].date),
        cause: it.name.city.Agra[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Agra[25].date),
        cause: it.name.city.Agra[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Agra[30].date),
        cause: it.name.city.Agra[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Agra[35].date),
        cause: it.name.city.Agra[35].active));

    confirm.add(it.name.city.Agra[35].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Aligarh[0].date),
        cause: it.name.city.Aligarh[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Aligarh[15].date),
        cause: it.name.city.Aligarh[15].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Aligarh[18].date),
        cause: it.name.city.Aligarh[18].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Aligarh[20].date),
        cause: it.name.city.Aligarh[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Aligarh[22].date),
        cause: it.name.city.Aligarh[22].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Aligarh[25].date),
        cause: it.name.city.Aligarh[25].active));

    confirm.add(it.name.city.Aligarh[25].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ambedkar_Nagar[0].date),
        cause: it.name.city.Ambedkar_Nagar[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ambedkar_Nagar[10].date),
        cause: it.name.city.Ambedkar_Nagar[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ambedkar_Nagar[15].date),
        cause: it.name.city.Ambedkar_Nagar[15].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ambedkar_Nagar[18].date),
        cause: it.name.city.Ambedkar_Nagar[18].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ambedkar_Nagar[20].date),
        cause: it.name.city.Ambedkar_Nagar[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ambedkar_Nagar[22].date),
        cause: it.name.city.Ambedkar_Nagar[22].active));

    confirm.add(it.name.city.Ambedkar_Nagar[22].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Amethi[0].date),
        cause: it.name.city.Amethi[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Amethi[10].date),
        cause: it.name.city.Amethi[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Amethi[18].date),
        cause: it.name.city.Amethi[18].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Amethi[20].date),
        cause: it.name.city.Amethi[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Amethi[24].date),
        cause: it.name.city.Amethi[24].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Amethi[26].date),
        cause: it.name.city.Amethi[26].active));

    confirm.add(it.name.city.Amethi[26].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Amroha[0].date),
        cause: it.name.city.Amroha[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Amroha[10].date),
        cause: it.name.city.Amroha[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Amroha[20].date),
        cause: it.name.city.Amroha[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Amroha[25].date),
        cause: it.name.city.Amroha[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Amroha[30].date),
        cause: it.name.city.Amroha[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Amroha[35].date),
        cause: it.name.city.Amroha[35].active));

    confirm.add(it.name.city.Amroha[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Auraiya[0].date),
        cause: it.name.city.Auraiya[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Auraiya[10].date),
        cause: it.name.city.Auraiya[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Auraiya[20].date),
        cause: it.name.city.Auraiya[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Auraiya[25].date),
        cause: it.name.city.Auraiya[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Auraiya[30].date),
        cause: it.name.city.Auraiya[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Auraiya[35].date),
        cause: it.name.city.Auraiya[35].active));

    confirm.add(it.name.city.Auraiya[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ayodhya[0].date),
        cause: it.name.city.Ayodhya[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ayodhya[10].date),
        cause: it.name.city.Ayodhya[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ayodhya[20].date),
        cause: it.name.city.Ayodhya[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ayodhya[25].date),
        cause: it.name.city.Ayodhya[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ayodhya[30].date),
        cause: it.name.city.Ayodhya[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ayodhya[35].date),
        cause: it.name.city.Ayodhya[35].active));

    confirm.add(it.name.city.Ayodhya[37].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Azamgarh[0].date),
        cause: it.name.city.Azamgarh[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Azamgarh[10].date),
        cause: it.name.city.Azamgarh[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Azamgarh[20].date),
        cause: it.name.city.Azamgarh[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Azamgarh[25].date),
        cause: it.name.city.Azamgarh[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Azamgarh[30].date),
        cause: it.name.city.Azamgarh[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Azamgarh[35].date),
        cause: it.name.city.Azamgarh[35].active));

    confirm.add(it.name.city.Azamgarh[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Baghpat[0].date),
        cause: it.name.city.Baghpat[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Baghpat[10].date),
        cause: it.name.city.Baghpat[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Baghpat[20].date),
        cause: it.name.city.Baghpat[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Baghpat[25].date),
        cause: it.name.city.Baghpat[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Baghpat[30].date),
        cause: it.name.city.Baghpat[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Baghpat[35].date),
        cause: it.name.city.Baghpat[35].active));

    confirm.add(it.name.city.Baghpat[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Balrampur[0].date),
        cause: it.name.city.Balrampur[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Balrampur[10].date),
        cause: it.name.city.Balrampur[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Balrampur[20].date),
        cause: it.name.city.Balrampur[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Balrampur[25].date),
        cause: it.name.city.Balrampur[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Balrampur[30].date),
        cause: it.name.city.Balrampur[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Balrampur[35].date),
        cause: it.name.city.Balrampur[35].active));

    confirm.add(it.name.city.Balrampur[40].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Banda[0].date),
        cause: it.name.city.Banda[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Banda[10].date),
        cause: it.name.city.Banda[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Banda[20].date),
        cause: it.name.city.Banda[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Banda[25].date),
        cause: it.name.city.Banda[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Banda[30].date),
        cause: it.name.city.Banda[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Banda[35].date),
        cause: it.name.city.Banda[35].active));

    confirm.add(it.name.city.Banda[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bara_Banki[0].date),
        cause: it.name.city.Bara_Banki[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bara_Banki[2].date),
        cause: it.name.city.Bara_Banki[2].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bara_Banki[4].date),
        cause: it.name.city.Bara_Banki[4].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bara_Banki[6].date),
        cause: it.name.city.Bara_Banki[6].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bara_Banki[8].date),
        cause: it.name.city.Bara_Banki[8].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bara_Banki[10].date),
        cause: it.name.city.Bara_Banki[10].active));

    confirm.add(it.name.city.Bara_Banki[10].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bareilly[0].date),
        cause: it.name.city.Bareilly[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bareilly[10].date),
        cause: it.name.city.Bareilly[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bareilly[20].date),
        cause: it.name.city.Bareilly[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bareilly[25].date),
        cause: it.name.city.Bareilly[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bareilly[30].date),
        cause: it.name.city.Bareilly[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bareilly[35].date),
        cause: it.name.city.Bareilly[35].active));

    confirm.add(it.name.city.Bareilly[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Basti[0].date),
        cause: it.name.city.Basti[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Basti[10].date),
        cause: it.name.city.Basti[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Basti[20].date),
        cause: it.name.city.Basti[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Basti[25].date),
        cause: it.name.city.Basti[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Basti[30].date),
        cause: it.name.city.Basti[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Basti[35].date),
        cause: it.name.city.Basti[35].active));

    confirm.add(it.name.city.Basti[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bhadohi[0].date),
        cause: it.name.city.Bhadohi[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bhadohi[10].date),
        cause: it.name.city.Bhadohi[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bhadohi[20].date),
        cause: it.name.city.Bhadohi[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bhadohi[25].date),
        cause: it.name.city.Bhadohi[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bhadohi[30].date),
        cause: it.name.city.Bhadohi[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Bhadohi[35].date),
        cause: it.name.city.Bhadohi[35].active));

    confirm.add(it.name.city.Bhadohi[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Etawah[0].date),
        cause: it.name.city.Etawah[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Etawah[10].date),
        cause: it.name.city.Etawah[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Etawah[20].date),
        cause: it.name.city.Etawah[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Etawah[25].date),
        cause: it.name.city.Etawah[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Etawah[30].date),
        cause: it.name.city.Etawah[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Etawah[35].date),
        cause: it.name.city.Etawah[35].active));

    confirm.add(it.name.city.Etawah[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Kanpur_Nagar[0].date),
        cause: it.name.city.Kanpur_Nagar[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Kanpur_Nagar[10].date),
        cause: it.name.city.Kanpur_Nagar[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Kanpur_Nagar[20].date),
        cause: it.name.city.Kanpur_Nagar[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Kanpur_Nagar[25].date),
        cause: it.name.city.Kanpur_Nagar[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Kanpur_Nagar[30].date),
        cause: it.name.city.Kanpur_Nagar[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Kanpur_Nagar[35].date),
        cause: it.name.city.Kanpur_Nagar[35].active));

    confirm.add(it.name.city.Kanpur_Nagar[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Pratapgarh[0].date),
        cause: it.name.city.Pratapgarh[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Pratapgarh[10].date),
        cause: it.name.city.Pratapgarh[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Pratapgarh[20].date),
        cause: it.name.city.Pratapgarh[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Pratapgarh[25].date),
        cause: it.name.city.Pratapgarh[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Pratapgarh[30].date),
        cause: it.name.city.Pratapgarh[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Pratapgarh[35].date),
        cause: it.name.city.Pratapgarh[35].active));

    confirm.add(it.name.city.Pratapgarh[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Prayagraj[0].date),
        cause: it.name.city.Prayagraj[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Prayagraj[10].date),
        cause: it.name.city.Prayagraj[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Prayagraj[20].date),
        cause: it.name.city.Prayagraj[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Prayagraj[25].date),
        cause: it.name.city.Prayagraj[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Prayagraj[30].date),
        cause: it.name.city.Prayagraj[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Prayagraj[35].date),
        cause: it.name.city.Prayagraj[35].active));

    confirm.add(it.name.city.Prayagraj[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Rae_Bareli[0].date),
        cause: it.name.city.Rae_Bareli[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Rae_Bareli[10].date),
        cause: it.name.city.Rae_Bareli[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Rae_Bareli[20].date),
        cause: it.name.city.Rae_Bareli[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Rae_Bareli[25].date),
        cause: it.name.city.Rae_Bareli[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Rae_Bareli[30].date),
        cause: it.name.city.Rae_Bareli[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Rae_Bareli[35].date),
        cause: it.name.city.Rae_Bareli[35].active));

    confirm.add(it.name.city.Rae_Bareli[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Varanasi[0].date),
        cause: it.name.city.Varanasi[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Varanasi[10].date),
        cause: it.name.city.Varanasi[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Varanasi[20].date),
        cause: it.name.city.Varanasi[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Varanasi[25].date),
        cause: it.name.city.Varanasi[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Varanasi[30].date),
        cause: it.name.city.Varanasi[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Varanasi[35].date),
        cause: it.name.city.Varanasi[35].active));

    confirm.add(it.name.city.Varanasi[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Lucknow[0].date),
        cause: it.name.city.Lucknow[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Lucknow[10].date),
        cause: it.name.city.Lucknow[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Lucknow[20].date),
        cause: it.name.city.Lucknow[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Lucknow[25].date),
        cause: it.name.city.Lucknow[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Lucknow[30].date),
        cause: it.name.city.Lucknow[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Lucknow[35].date),
        cause: it.name.city.Lucknow[35].active));

    confirm.add(it.name.city.Lucknow[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ghaziabad[0].date),
        cause: it.name.city.Ghaziabad[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ghaziabad[10].date),
        cause: it.name.city.Ghaziabad[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ghaziabad[20].date),
        cause: it.name.city.Ghaziabad[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ghaziabad[25].date),
        cause: it.name.city.Ghaziabad[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ghaziabad[30].date),
        cause: it.name.city.Ghaziabad[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Ghaziabad[35].date),
        cause: it.name.city.Ghaziabad[35].active));

    confirm.add(it.name.city.Ghaziabad[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Hardoi[0].date),
        cause: it.name.city.Hardoi[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Hardoi[10].date),
        cause: it.name.city.Hardoi[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Hardoi[20].date),
        cause: it.name.city.Hardoi[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Hardoi[25].date),
        cause: it.name.city.Hardoi[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Hardoi[30].date),
        cause: it.name.city.Hardoi[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Hardoi[35].date),
        cause: it.name.city.Hardoi[35].active));

    confirm.add(it.name.city.Hardoi[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Jhansi[0].date),
        cause: it.name.city.Jhansi[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Jhansi[10].date),
        cause: it.name.city.Jhansi[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Jhansi[20].date),
        cause: it.name.city.Jhansi[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Jhansi[25].date),
        cause: it.name.city.Jhansi[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Jhansi[30].date),
        cause: it.name.city.Jhansi[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Jhansi[34].date),
        cause: it.name.city.Jhansi[34].active));

    confirm.add(it.name.city.Jhansi[35].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Meerut[0].date),
        cause: it.name.city.Meerut[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Meerut[10].date),
        cause: it.name.city.Meerut[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Meerut[20].date),
        cause: it.name.city.Meerut[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Meerut[25].date),
        cause: it.name.city.Meerut[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Meerut[30].date),
        cause: it.name.city.Meerut[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Meerut[35].date),
        cause: it.name.city.Meerut[35].active));

    confirm.add(it.name.city.Meerut[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Jaunpur[0].date),
        cause: it.name.city.Jaunpur[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Jaunpur[10].date),
        cause: it.name.city.Jaunpur[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Jaunpur[20].date),
        cause: it.name.city.Jaunpur[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Jaunpur[25].date),
        cause: it.name.city.Jaunpur[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Jaunpur[30].date),
        cause: it.name.city.Jaunpur[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Jaunpur[35].date),
        cause: it.name.city.Jaunpur[35].active));

    confirm.add(it.name.city.Jaunpur[42].confirmed);

    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Mathura[0].date),
        cause: it.name.city.Mathura[0].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Mathura[10].date),
        cause: it.name.city.Mathura[10].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Mathura[20].date),
        cause: it.name.city.Mathura[20].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Mathura[25].date),
        cause: it.name.city.Mathura[25].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Mathura[30].date),
        cause: it.name.city.Mathura[30].active));
    getlist.add(LineChartStructure(date: DateTime.parse(it.name.city.Mathura[35].date),
        cause: it.name.city.Mathura[35].active));

    confirm.add(it.name.city.Mathura[42].confirmed);

    print("-----------------------------------");
    print(confirm.length);
    print(getlist.length);
    print(getlist[0].date.toString());
    setState(() {
      desktopSales = getlist;
    });
    return desktopSales;
  }

  Widget Chart_Decorator_new(String city, int val,bool flag){
    return AnimatedContainer(
      duration: Duration(milliseconds: 100),
      curve: Curves.bounceIn,
      margin: EdgeInsets.only(top: flag?20 :80,bottom: 30,left: 5, right: 15),
      child: Card(
        color: const Color(0xffDAEDFE),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
              bottomLeft:Radius.circular(20),
              bottomRight: Radius.circular(20)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(left: 16,top: 16),
                child: Create_card("Statistics")),
            Expanded(
              child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(top: 40,bottom: 40,left: 15, right: 15),
                  color: Color(0xffEFF8FF),
                  child: list_series(cityname: city,name: city_name.values[val])
              ),
            ),
          ],
        ),
      ) ,
      decoration: new BoxDecoration(boxShadow: [
        new BoxShadow(
          color: Colors.black26,
          blurRadius: 8.0,
        ),
      ]),
    );
  }


  Widget Chart_Decorator(String city, int val, List<LineChartStructure> list,String value){
    return AnimatedContainer(
      height: 80,
      width: 200,
      duration: Duration(milliseconds: 500),
      margin: EdgeInsets.only(top: 20,left: 16, right: 4,bottom: 0),
      child: Card(
        color: Color(0xffFF4E4E),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
              bottomLeft:Radius.circular(20),
              bottomRight: Radius.circular(20)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(top: 4),
                child: Text(city,style: TextStyle(color: Colors.white,fontSize: 18,fontWeight: FontWeight.bold))),
            Expanded(
              child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(top: 5,bottom: 5,left: 10, right: 10),
                  color: Color(0xffFF4E4E),
                  child: Line_Chart(cityname: city,name: city_name_date.values[val],mydesktopSales: list,value: value,)
              ),
            ),
          ],
        ),
      ) ,
      decoration: new BoxDecoration(boxShadow: [
        new BoxShadow(
          color: Colors.black26,
          blurRadius: 6.0,
          spreadRadius: 0.6
        ),
      ]),
    );
  }

  Widget Create_card(String text) {
    return Container(
      width: 100,
      padding: EdgeInsets.only(bottom: 5, top: 5),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            Colors.blue,
            Colors.lightBlueAccent,
          ],
        ),
        borderRadius: BorderRadius.circular(32),
      ),
      child: Center(
          child: Text(
            text,
            style: TextStyle(fontSize: 16, color: Colors.white),
          )),
    );
  }

}

/// Sample ordinal data type.
