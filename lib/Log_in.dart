import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


//void main() {
//  runApp(Log_in());
//}

class Log_in extends StatefulWidget{
  Log_In createState() => Log_In();
}

class Log_In extends State<Log_in>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color(0xffB6D2EB),
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Image.asset('asset/Images/Social_dist_1.png'),
              spacing(18),
              Expanded(
                child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40)),
                      ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        spacing(16),
                        Container(
                          //margin: EdgeInsets.only(left: 0),
                          height: 70,
                          child: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                      child: Text("Name",style: TextStyle(fontWeight: FontWeight.bold,color: Color(0xff0494D2)))),
                                  spacing(8),
                                  InputBox("Name")
                                ],
                              ),
                            ],
                          ),
                        ),
                        spacing(16),
                        Container(
                          //margin: EdgeInsets.only(left: 0),
                          height: 70,
                          child: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: Text("Email",style: TextStyle(fontWeight: FontWeight.bold,color: Color(0xff0494D2)))),
                                  spacing(8),
                                  InputBox("Email")
                                ],
                              ),
                            ],
                          ),
                        ),
                        spacing(16),
                        Container(
                          //margin: EdgeInsets.only(left: 0),
                          height: 70,
                          child: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: Text("Password",style: TextStyle(fontWeight: FontWeight.bold,
                                          color: Color(0xff0494D2)))),
                                  spacing(8),
                                  InputBox("Password")
                                ],
                              ),
                            ],
                          ),
                        ),
                        spacing(20),
                        Container(
                          alignment: Alignment.center,
                          width: 320,
                          height: 50,
                          margin: EdgeInsets.only(left: 16, right: 16),
                          padding: EdgeInsets.only(left: 8),
                          decoration: BoxDecoration(
                            color: Color(0xff0494D2),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Text("Get Started",style: TextStyle(color: Colors.white),),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget InputBox(String textile) {
    return Expanded(
      child: Container(
        width: 320,
        margin: EdgeInsets.only(left: 16, right: 16),
        padding: EdgeInsets.only(left: 8),
        //color: Color(0xFF2058E8),
        //color: Color(0xFFFDDB89),
        decoration: BoxDecoration(
          color: Color(0xFFD6D6D6),
          borderRadius: BorderRadius.circular(20),
//          boxShadow: [
//            BoxShadow(
//                color: Colors.black45,
//                blurRadius: 5,
//                spreadRadius: 2,
//                offset: Offset(0, 3)
//            )
//          ],
        ),
//  collapsed to remove underline and hint text
        child: TextFormField(
          //validator: validator,
          //controller: controller,
          decoration: InputDecoration.collapsed(
            //contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
            hintText: textile,
          ),
          keyboardType: TextInputType.text,
        ),
      ),
    );
  }


  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }


}