
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'Api/Fetched_Data.dart';
import 'package:xdtester/Charts/Charts_Structure_body.dart';
//import 'Api/test_retro.dart';

//void main() {
//  runApp(Tab_bar());
//}

class Tab_bar extends StatefulWidget{
  Tabs createState() => Tabs();
}

class Tabs extends State<Tab_bar> with TickerProviderStateMixin {
  TabController controller;
  Animation animation;
  AnimationController an_controller;

  @override
  void initState() {
    controller = TabController(length: 2,vsync: this);
    an_controller = AnimationController(vsync: this,duration: Duration(seconds: 2));
    animation = Tween<double>(begin: 1.0, end: 0.0).animate(CurvedAnimation(parent: an_controller,
        curve: Curves.fastOutSlowIn));
    an_controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
            child: Column(
              children: <Widget>[
                myTabBar(controller)
              ],
            ),
          ),
        ),
      );
  }

  Widget myTabBar(TabController controller){
    return Column(
      children: <Widget>[
        Card(
          color: Color(0xffDAEDFE),
          child: TabBar(
            //indicatorPadding: EdgeInsets.only(top: 8,bottom: 8),
            labelPadding: EdgeInsets.only(top: 8,bottom: 8),
            //isScrollable: true,
            unselectedLabelColor: Colors.black45,
            indicator: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
                colors: [
                  Colors.lightBlueAccent,
                  Colors.blue,
                ],
              ),
              borderRadius: BorderRadius.circular(32),
            ),
            controller: controller,
            tabs: <Widget>[
              Text('Precautions'),
              Text('Symptoms')
            ],
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(32)),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 16,right: 16),
          decoration: BoxDecoration(
            color: Color(0xffDAEDFE),
            borderRadius: BorderRadius.circular(16),
          ),
          height: 360,
          child: TabBarView(
            controller: controller,
            children: <Widget>[
              firstTab(),
              SecondTab()
            ],
          ),
        )
      ],
    );
  }

  Widget firstTab(){
    return GridView.count(
      padding: EdgeInsets.only(left: 8,right: 8,top: 8),
      childAspectRatio: 1.5,
      mainAxisSpacing: 25,
      crossAxisSpacing: 25,
      crossAxisCount: 2,
      children: <Widget>[
        Container(
            decoration: BoxDecoration(
              color: Color(0xffEFF8FF),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Image.asset('assets/Images/Mask.png')),
        Container(decoration: BoxDecoration(
          color: Color(0xffEFF8FF),
          borderRadius: BorderRadius.circular(20),
        ),
            child: Image.asset('assets/Images/Dont Shake hands.png')),
        Container(
            decoration: BoxDecoration(
              color: Color(0xffEFF8FF),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Image.asset('assets/Images/Wash hand.png')),
        Container(
            decoration: BoxDecoration(
              color: Color(0xffEFF8FF),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Image.asset('assets/Images/Dont Touch Eyes.png')),
        Container(
            decoration: BoxDecoration(
              color: Color(0xffEFF8FF),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Image.asset('assets/Images/Cook Food Throughly.png')),
        Container(
            decoration: BoxDecoration(
              color: Color(0xffEFF8FF),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Image.asset('assets/Images/Dont travel.png'))
      ]
    );
  }

  Widget SecondTab(){
    return GridView.count(
        padding: EdgeInsets.only(left: 8,right: 8,top: 8),
        childAspectRatio: 1.5,
        mainAxisSpacing: 25,
        crossAxisSpacing: 25,
        crossAxisCount: 2,
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                color: Color(0xffEFF8FF),
                borderRadius: BorderRadius.circular(20),
              ),
              child: Image.asset('assets/Images/High Temp.png')),
          Container(decoration: BoxDecoration(
            color: Color(0xffEFF8FF),
            borderRadius: BorderRadius.circular(20),
          ),
              child: Image.asset('assets/Images/Cough.png')),
          Container(
              decoration: BoxDecoration(
                color: Color(0xffEFF8FF),
                borderRadius: BorderRadius.circular(20),
              ),
              child: Image.asset('assets/Images/Rashesh.png')),
          Container(
              decoration: BoxDecoration(
                color: Color(0xffEFF8FF),
                borderRadius: BorderRadius.circular(20),
              ),
              child: Image.asset('assets/Images/Shivring.png')),
          Container(
              decoration: BoxDecoration(
                color: Color(0xffEFF8FF),
                borderRadius: BorderRadius.circular(20),
              ),
              child: Image.asset('assets/Images/Cough & Cold.png')),
          Container(
              decoration: BoxDecoration(
                color: Color(0xffEFF8FF),
                borderRadius: BorderRadius.circular(20),
              ),
              child: Image.asset('assets/Images/Head Ache.png'))
        ]
    );
  }

}