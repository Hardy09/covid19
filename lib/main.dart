import 'package:backdrop/backdrop.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Dashboard.dart';
import 'package:xdtester/Charts/game.dart';
import 'package:xdtester/Bluetooth/blue.dart';
import 'package:xdtester/Location/location.dart';

void main() {
  runApp(demo());
}

class demo extends StatefulWidget{
  int position;
  bool flag;
  demo({this.position,this.flag});
  Demo_Again createState() => Demo_Again();
}

class Demo_Again extends State<demo> with SingleTickerProviderStateMixin{
  TabController controller;
  int currentIndex =0;
  final dash = dashboard();
  final analysis = SimpleLineChartState();
  List<Widget> pages = new List();

  @override
  void initState() {
    super.initState();
    widget.flag = false;
    if(widget.position == 1)
      widget.flag = true;
    controller = TabController(length: 2,vsync: this);
      pages = [Dashboard(),SampleBarChart(),blue(),lllocation()];
      if(widget.flag){
        currentIndex = widget.position;
      }else{
        currentIndex =0;
      }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xffB6D2EB)
      ),
      home: Scaffold(
        backgroundColor: Color(0xffB6D2EB),
        body: SafeArea(
          child: Column(
            children: <Widget>[
                Container(
                  height: 615,
                  child: BackdropScaffold(
                    title: Text("Socia",style: TextStyle(color: Colors.white)),
                    frontLayerBorderRadius: BorderRadius.circular(50),
                    headerHeight: 300,
                    actions: <Widget>[
                    ],
                    frontLayer: pages[currentIndex],
                    backLayer: BackdropNavigationBackLayer(
                      items: [
                        ListTile(title: Row(
                          children: <Widget>[
                            Image.asset("assets/Images/Home_Icon.png",width: 18,height: 18),
                            spacing(18),
                            Text("Home",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white)),
                          ],
                        ),
                        ),
                        ListTile(title: Row(
                          children: <Widget>[
                            Icon(Icons.transfer_within_a_station,color: Colors.blue),
                            spacing(15),
                            Text("Analysis",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white)),
                          ],
                        ),
                        ),
                        ListTile(title: Row(
                          children: <Widget>[
                            Icon(Icons.bluetooth,color: Colors.blue),
                            spacing(15),
                            Text("Distancing",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white)),
                          ],
                        ),
                        ),
                        ListTile(title: Row(
                          children: <Widget>[
                            Icon(Icons.map,color: Colors.blue),
                            spacing(15),
                            Text("Location",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white)),
                          ],
                        ),
                        ),
                      ],
                      onTap: (position) => {
                        getPosition(position)
                      },
                    ),
                    //iconPosition: BackdropIconPosition.leading,
                    //resizeToAvoidBottomInset: true,
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }

  int getPosition(int position){
    setState(() {
      currentIndex = position;
      return currentIndex;
    });
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(right: x),
    );
  }

}