import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';


//void main() => runApp(lllocation());

class lllocation extends StatefulWidget {
  llocation createState() => llocation();
}

class llocation extends State {
  Position currentPosition;
  String currentAddress;
  LocationData _locationData;
  Location location = new Location();
  Widget myform;
  GoogleMapController _controller;
  int currentZoom = 10;
  List<Marker> markers = [];
  List<List> markerLocations = [];
  BitmapDescriptor myIcon;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager=true;
  final databaseReference = Firestore.instance;
  int count = 0;

  @override
  void initState() {
    super.initState();
    Currlocation();
    BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(480, 480)), 'assets/Images/corona_icon.png')
        .then((onValue) {
      myIcon = onValue;
    });
  }

  void OnMapCreated (GoogleMapController controller) {
        _controller = controller;
        print("length of markers list"+markerLocations.length.toString());
        markerLocations.forEach((element) {
          print("Marker_Location"+element.toString());
          setState(() {
              markers.add(
                Marker(
                  markerId: MarkerId(markerLocations.indexOf(element).toString()),
                  position: LatLng(element[1],element[0]),
                  icon: myIcon,
                )
              );
          });
        });
        print("Data in markers"+ markers.length.toString());
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color(0xffB6D2EB),
        body: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                  bottomLeft: Radius.circular(40),
                  bottomRight: Radius.circular(40)),
            ),
            child: Google()),
        )
    );
  }

  Widget Google() {
      print("In Google");
      if(count > 0) {
        return GoogleMap(
          initialCameraPosition: CameraPosition(
            target: LatLng(26.880134, 80.972002),
            zoom: 15,
          ),
          markers: markers.toSet(),
          onMapCreated: (controller) {
            OnMapCreated(controller);
          },
        );
      }else {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
  }

  Future userLocation(double latitude,double longitude) async{
    await databaseReference.collection("Positions").document().setData({
      "Latitude" : latitude,
      "Longitude" : longitude,
    }).then((onValue){
      print("Data successfully Entered");
      getOrdersDashboard();
    }).catchError((onError){
      print(onError);
    });
  }

  Future<List<DocumentSnapshot>> getOrdersDashboard() async{
    QuerySnapshot snapshot = await databaseReference.collection("Positions").getDocuments();
    snapshot.documents.forEach((element) {
      setState(() {
        markerLocations.add(element.data.values.toList());
        count++;
      });
    });
    print("In Marker Location"+markerLocations.toString());
  }

  Future<Widget> Currlocation() async{
    try{
      _locationData = await location.getLocation();
      print("Current loc");
      print(_locationData);
      userLocation(_locationData.latitude, _locationData.longitude);
      }on Exception catch(e){
          print('Could not get location: ${e.toString()}');
      }
    }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }

  Widget padding(double x) {
    return Container(
      padding: EdgeInsets.only(top: x),
    );
  }

}





