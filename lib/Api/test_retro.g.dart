// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_retro.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Source _$SourceFromJson(Map<String, dynamic> json) {
  return Source(
    used: json['used'] as bool,
    source: json['source'] as String,
    type: json['type'] as String,
    id: json['id'] as String,
    text: json['text'] as String,
    deleted: json['deleted'] as bool,
    status: json['status'] == null
        ? null
        : Status.fromJson(json['status'] as Map<String, dynamic>),
    user: json['user'] as String,
  );
}

Map<String, dynamic> _$SourceToJson(Source instance) => <String, dynamic>{
      'used': instance.used,
      'source': instance.source,
      'type': instance.type,
      'deleted': instance.deleted,
      'id': instance.id,
      'text': instance.text,
      'status': instance.status,
      'user': instance.user,
    };

Status _$StatusFromJson(Map<String, dynamic> json) {
  return Status(
    sentCount: json['sentCount'] as int,
    verified: json['verified'] as bool,
  );
}

Map<String, dynamic> _$StatusToJson(Status instance) => <String, dynamic>{
      'verified': instance.verified,
      'sentCount': instance.sentCount,
    };

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _RestClient implements RestClient {
  _RestClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    this.baseUrl ??= 'https://cat-fact.herokuapp.com/';
  }

  final Dio _dio;

  String baseUrl;

  @override
  getData() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<List<dynamic>> _result = await _dio.request(
        '/facts/random?animal_type=cat&amount=5',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    var value = _result.data
        .map((dynamic i) => Source.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }
}
