import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'Date_wise_data.g.dart';

@RestApi(baseUrl: "https://api.covid19india.org/")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET("/districts_daily.json")
  Future<DistrictData> getDateWiseData();
}

@JsonSerializable()
class DistrictData {
  @JsonKey(name:"districtsDaily", required: true)
  StateName name;
  DistrictData({this.name});

  factory DistrictData.fromJson(Map<String, dynamic> json) => _$DistrictDataFromJson(json);
  Map<String, dynamic> toJson() => _$DistrictDataToJson(this);

}

@JsonSerializable()
class StateName{
  @JsonKey(name:"Uttar Pradesh",required: true)
  CityName city;

  StateName({this.city});

  factory StateName.fromJson(Map<String, dynamic> json) => _$StateNameFromJson(json);
  Map<String, dynamic> toJson() => _$StateNameToJson(this);
}

@JsonSerializable()
class CityName{
  @JsonKey(name:"Agra",required: true)
  List<Obj> Agra;

  @JsonKey(name:"Aligarh",required: true)
  List<Obj> Aligarh;

  @JsonKey(name:"Ambedkar Nagar",required: true)
  List<Obj> Ambedkar_Nagar;

  @JsonKey(name:"Amethi",required: true)
  List<Obj> Amethi;

  @JsonKey(name:"Amroha",required: true)
  List<Obj> Amroha;

  @JsonKey(name:"Auraiya",required: true)
  List<Obj> Auraiya;

  @JsonKey(name:"Ayodhya",required: true)
  List<Obj> Ayodhya;

  @JsonKey(name:"Azamgarh",required: true)
  List<Obj> Azamgarh;

  @JsonKey(name:"Baghpat",required: true)
  List<Obj> Baghpat;

  @JsonKey(name:"Balrampur",required: true)
  List<Obj> Balrampur;

  @JsonKey(name:"Banda",required: true)
  List<Obj> Banda;

  @JsonKey(name:"Bara Banki",required: true)
  List<Obj> Bara_Banki;

  @JsonKey(name:"Bareilly",required: true)
  List<Obj> Bareilly;

  @JsonKey(name:"Basti",required: true)
  List<Obj> Basti;

  @JsonKey(name:"Bhadohi",required: true)
  List<Obj> Bhadohi;

  @JsonKey(name:"Etawah",required: true)
  List<Obj> Etawah;

  @JsonKey(name:"Kanpur Nagar",required: true)
  List<Obj> Kanpur_Nagar;

  @JsonKey(name:"Pratapgarh",required: true)
  List<Obj> Pratapgarh;

  @JsonKey(name:"Prayagraj",required: true)
  List<Obj> Prayagraj;

  @JsonKey(name:"Rae Bareli",required: true)
  List<Obj> Rae_Bareli;

  @JsonKey(name:"Varanasi",required: true)
  List<Obj> Varanasi;

  @JsonKey(name:"Lucknow",required: true)
  List<Obj> Lucknow;

  @JsonKey(name:"Ghaziabad",required: true)
  List<Obj> Ghaziabad;

  @JsonKey(name:"Hardoi",required: true)
  List<Obj> Hardoi;

  @JsonKey(name:"Jhansi",required: true)
  List<Obj> Jhansi;

  @JsonKey(name:"Meerut",required: true)
  List<Obj> Meerut;

  @JsonKey(name:"Jaunpur",required: true)
  List<Obj> Jaunpur;

  @JsonKey(name:"Mathura",required: true)
  List<Obj> Mathura;


  CityName({this.Agra,this.Aligarh,this.Ambedkar_Nagar,this.Amethi,this.Amroha,this.Auraiya,this.Ayodhya,
  this.Azamgarh,this.Baghpat,this.Balrampur,this.Banda,this.Bara_Banki,this.Bareilly,this.Basti,this.Bhadohi,
  this.Etawah,this.Ghaziabad,this.Hardoi,this.Jaunpur,this.Jhansi,this.Kanpur_Nagar,this.Lucknow,this.Mathura,
  this.Meerut,this.Pratapgarh,this.Prayagraj,this.Rae_Bareli,this.Varanasi});

  factory CityName.fromJson(Map<String, dynamic> json) => _$CityNameFromJson(json);
  Map<String, dynamic> toJson() => _$CityNameToJson(this);
}

@JsonSerializable()
class Obj{
  String date;
  int active;
  int confirmed;
  int deceased;
  int recovered;

  Obj({this.date,this.confirmed,this.recovered,this.deceased,this.active});

  factory Obj.fromJson(Map<String, dynamic> json) => _$ObjFromJson(json);
  Map<String, dynamic> toJson() => _$ObjToJson(this);
}