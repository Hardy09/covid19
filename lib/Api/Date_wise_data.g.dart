// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Date_wise_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DistrictData _$DistrictDataFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['districtsDaily']);
  return DistrictData(
    name: json['districtsDaily'] == null
        ? null
        : StateName.fromJson(json['districtsDaily'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DistrictDataToJson(DistrictData instance) =>
    <String, dynamic>{
      'districtsDaily': instance.name,
    };

StateName _$StateNameFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['Uttar Pradesh']);
  return StateName(
    city: json['Uttar Pradesh'] == null
        ? null
        : CityName.fromJson(json['Uttar Pradesh'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$StateNameToJson(StateName instance) => <String, dynamic>{
      'Uttar Pradesh': instance.city,
    };

CityName _$CityNameFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const [
    'Agra',
    'Aligarh',
    'Ambedkar Nagar',
    'Amethi',
    'Amroha',
    'Auraiya',
    'Ayodhya',
    'Azamgarh',
    'Baghpat',
    'Balrampur',
    'Banda',
    'Bara Banki',
    'Bareilly',
    'Basti',
    'Bhadohi',
    'Etawah',
    'Kanpur Nagar',
    'Pratapgarh',
    'Prayagraj',
    'Rae Bareli',
    'Varanasi',
    'Lucknow',
    'Ghaziabad',
    'Hardoi',
    'Jhansi',
    'Meerut',
    'Jaunpur',
    'Mathura'
  ]);
  return CityName(
    Agra: (json['Agra'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Aligarh: (json['Aligarh'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Ambedkar_Nagar: (json['Ambedkar Nagar'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Amethi: (json['Amethi'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Amroha: (json['Amroha'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Auraiya: (json['Auraiya'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Ayodhya: (json['Ayodhya'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Azamgarh: (json['Azamgarh'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Baghpat: (json['Baghpat'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Balrampur: (json['Balrampur'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Banda: (json['Banda'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Bara_Banki: (json['Bara Banki'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Bareilly: (json['Bareilly'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Basti: (json['Basti'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Bhadohi: (json['Bhadohi'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Etawah: (json['Etawah'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Ghaziabad: (json['Ghaziabad'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Hardoi: (json['Hardoi'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Jaunpur: (json['Jaunpur'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Jhansi: (json['Jhansi'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Kanpur_Nagar: (json['Kanpur Nagar'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Lucknow: (json['Lucknow'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Mathura: (json['Mathura'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Meerut: (json['Meerut'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Pratapgarh: (json['Pratapgarh'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Prayagraj: (json['Prayagraj'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Rae_Bareli: (json['Rae Bareli'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    Varanasi: (json['Varanasi'] as List)
        ?.map((e) => e == null ? null : Obj.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CityNameToJson(CityName instance) => <String, dynamic>{
      'Agra': instance.Agra,
      'Aligarh': instance.Aligarh,
      'Ambedkar Nagar': instance.Ambedkar_Nagar,
      'Amethi': instance.Amethi,
      'Amroha': instance.Amroha,
      'Auraiya': instance.Auraiya,
      'Ayodhya': instance.Ayodhya,
      'Azamgarh': instance.Azamgarh,
      'Baghpat': instance.Baghpat,
      'Balrampur': instance.Balrampur,
      'Banda': instance.Banda,
      'Bara Banki': instance.Bara_Banki,
      'Bareilly': instance.Bareilly,
      'Basti': instance.Basti,
      'Bhadohi': instance.Bhadohi,
      'Etawah': instance.Etawah,
      'Kanpur Nagar': instance.Kanpur_Nagar,
      'Pratapgarh': instance.Pratapgarh,
      'Prayagraj': instance.Prayagraj,
      'Rae Bareli': instance.Rae_Bareli,
      'Varanasi': instance.Varanasi,
      'Lucknow': instance.Lucknow,
      'Ghaziabad': instance.Ghaziabad,
      'Hardoi': instance.Hardoi,
      'Jhansi': instance.Jhansi,
      'Meerut': instance.Meerut,
      'Jaunpur': instance.Jaunpur,
      'Mathura': instance.Mathura,
    };

Obj _$ObjFromJson(Map<String, dynamic> json) {
  return Obj(
    date: json['date'] as String,
    confirmed: json['confirmed'] as int,
    recovered: json['recovered'] as int,
    deceased: json['deceased'] as int,
    active: json['active'] as int,
  );
}

Map<String, dynamic> _$ObjToJson(Obj instance) => <String, dynamic>{
      'date': instance.date,
      'active': instance.active,
      'confirmed': instance.confirmed,
      'deceased': instance.deceased,
      'recovered': instance.recovered,
    };

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _RestClient implements RestClient {
  _RestClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    this.baseUrl ??= 'https://api.covid19india.org/';
  }

  final Dio _dio;

  String baseUrl;

  @override
  getDateWiseData() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/districts_daily.json',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = DistrictData.fromJson(_result.data);
    return value;
  }
}
