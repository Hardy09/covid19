// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Fetched_Data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MyCountry _$MyCountryFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['Uttar Pradesh']);
  return MyCountry(
    myState: json['Uttar Pradesh'] == null
        ? null
        : MyState.fromJson(json['Uttar Pradesh'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$MyCountryToJson(MyCountry instance) => <String, dynamic>{
      'Uttar Pradesh': instance.myState,
    };

MyState _$MyStateFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['statecode', 'districtData']);
  return MyState(
    stateCode: json['statecode'] as String,
    data: json['districtData'] == null
        ? null
        : DistrictData.fromJson(json['districtData'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$MyStateToJson(MyState instance) => <String, dynamic>{
      'statecode': instance.stateCode,
      'districtData': instance.data,
    };

DistrictData _$DistrictDataFromJson(Map<String, dynamic> json) {
  return DistrictData(
    Agra: json['Agra'] == null
        ? null
        : CityData.fromJson(json['Agra'] as Map<String, dynamic>),
    aligarh: json['Aligarh'] == null
        ? null
        : CityData.fromJson(json['Aligarh'] as Map<String, dynamic>),
    amber_nagar: json['Ambedkar Nagar'] == null
        ? null
        : CityData.fromJson(json['Ambedkar Nagar'] as Map<String, dynamic>),
    amethi: json['Amethi'] == null
        ? null
        : CityData.fromJson(json['Amethi'] as Map<String, dynamic>),
    amroha: json['Amroha'] == null
        ? null
        : CityData.fromJson(json['Amroha'] as Map<String, dynamic>),
    auriya: json['Auraiya'] == null
        ? null
        : CityData.fromJson(json['Auraiya'] as Map<String, dynamic>),
    ayodhya: json['Ayodhya'] == null
        ? null
        : CityData.fromJson(json['Ayodhya'] as Map<String, dynamic>),
    azamgarh: json['Azamgarh'] == null
        ? null
        : CityData.fromJson(json['Azamgarh'] as Map<String, dynamic>),
    bagpath: json['Baghpat'] == null
        ? null
        : CityData.fromJson(json['Baghpat'] as Map<String, dynamic>),
    balrampur: json['Balrampur'] == null
        ? null
        : CityData.fromJson(json['Balrampur'] as Map<String, dynamic>),
    banda: json['Banda'] == null
        ? null
        : CityData.fromJson(json['Banda'] as Map<String, dynamic>),
    barabanki: json['Barabanki'] == null
        ? null
        : CityData.fromJson(json['Barabanki'] as Map<String, dynamic>),
    bareilly: json['Bareilly'] == null
        ? null
        : CityData.fromJson(json['Bareilly'] as Map<String, dynamic>),
    basti: json['Basti'] == null
        ? null
        : CityData.fromJson(json['Basti'] as Map<String, dynamic>),
    bhadohi: json['Bhadohi'] == null
        ? null
        : CityData.fromJson(json['Bhadohi'] as Map<String, dynamic>),
    bijnor: json['Bijnor'] == null
        ? null
        : CityData.fromJson(json['Bijnor'] as Map<String, dynamic>),
    budaun: json['Budaun'] == null
        ? null
        : CityData.fromJson(json['Budaun'] as Map<String, dynamic>),
    bulandsahr: json['Bulandshahr'] == null
        ? null
        : CityData.fromJson(json['Bulandshahr'] as Map<String, dynamic>),
    chandauli: json['Chandauli'] == null
        ? null
        : CityData.fromJson(json['Chandauli'] as Map<String, dynamic>),
    chitrakoot: json['Chitrakoot'] == null
        ? null
        : CityData.fromJson(json['Chitrakoot'] as Map<String, dynamic>),
    etawah: json['Etawah'] == null
        ? null
        : CityData.fromJson(json['Etawah'] as Map<String, dynamic>),
    farrukhabad: json['Farrukhabad'] == null
        ? null
        : CityData.fromJson(json['Farrukhabad'] as Map<String, dynamic>),
    firozabad: json['Firozabad'] == null
        ? null
        : CityData.fromJson(json['Firozabad'] as Map<String, dynamic>),
    ghaziabad: json['Ghaziabad'] == null
        ? null
        : CityData.fromJson(json['Ghaziabad'] as Map<String, dynamic>),
    gonda: json['Gonda'] == null
        ? null
        : CityData.fromJson(json['Gonda'] as Map<String, dynamic>),
    hapur: json['Hapur'] == null
        ? null
        : CityData.fromJson(json['Hapur'] as Map<String, dynamic>),
    kanpur: json['Kanpur Nagar'] == null
        ? null
        : CityData.fromJson(json['Kanpur Nagar'] as Map<String, dynamic>),
    lucknow: json['Lucknow'] == null
        ? null
        : CityData.fromJson(json['Lucknow'] as Map<String, dynamic>),
    meerut: json['Meerut'] == null
        ? null
        : CityData.fromJson(json['Meerut'] as Map<String, dynamic>),
    moradabad: json['Moradabad'] == null
        ? null
        : CityData.fromJson(json['Moradabad'] as Map<String, dynamic>),
    pratapgarh: json['Pratapgarh'] == null
        ? null
        : CityData.fromJson(json['Pratapgarh'] as Map<String, dynamic>),
    prayagraj: json['Prayagraj'] == null
        ? null
        : CityData.fromJson(json['Prayagraj'] as Map<String, dynamic>),
    raeBareli: json['Rae Bareli'] == null
        ? null
        : CityData.fromJson(json['Rae Bareli'] as Map<String, dynamic>),
    rampur: json['Rampur'] == null
        ? null
        : CityData.fromJson(json['Rampur'] as Map<String, dynamic>),
    sitapur: json['Sitapur'] == null
        ? null
        : CityData.fromJson(json['Sitapur'] as Map<String, dynamic>),
    unnao: json['Unnao'] == null
        ? null
        : CityData.fromJson(json['Unnao'] as Map<String, dynamic>),
    varanasi: json['Varanasi'] == null
        ? null
        : CityData.fromJson(json['Varanasi'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DistrictDataToJson(DistrictData instance) =>
    <String, dynamic>{
      'Agra': instance.Agra,
      'Aligarh': instance.aligarh,
      'Ambedkar Nagar': instance.amber_nagar,
      'Amethi': instance.amethi,
      'Amroha': instance.amroha,
      'Auraiya': instance.auriya,
      'Ayodhya': instance.ayodhya,
      'Azamgarh': instance.azamgarh,
      'Baghpat': instance.bagpath,
      'Balrampur': instance.balrampur,
      'Banda': instance.banda,
      'Barabanki': instance.barabanki,
      'Bareilly': instance.bareilly,
      'Basti': instance.basti,
      'Bhadohi': instance.bhadohi,
      'Bijnor': instance.bijnor,
      'Budaun': instance.budaun,
      'Bulandshahr': instance.bulandsahr,
      'Chandauli': instance.chandauli,
      'Chitrakoot': instance.chitrakoot,
      'Etawah': instance.etawah,
      'Farrukhabad': instance.farrukhabad,
      'Firozabad': instance.firozabad,
      'Ghaziabad': instance.ghaziabad,
      'Gonda': instance.gonda,
      'Hapur': instance.hapur,
      'Kanpur Nagar': instance.kanpur,
      'Lucknow': instance.lucknow,
      'Meerut': instance.meerut,
      'Moradabad': instance.moradabad,
      'Pratapgarh': instance.pratapgarh,
      'Prayagraj': instance.prayagraj,
      'Rae Bareli': instance.raeBareli,
      'Rampur': instance.rampur,
      'Sitapur': instance.sitapur,
      'Unnao': instance.unnao,
      'Varanasi': instance.varanasi,
    };

CityData _$CityDataFromJson(Map<String, dynamic> json) {
  return CityData(
    recovered: json['recovered'] as int,
    deceased: json['deceased'] as int,
    confirmed: json['confirmed'] as int,
    active: json['active'] as int,
    delta: json['delta'] == null
        ? null
        : Delta.fromJson(json['delta'] as Map<String, dynamic>),
    notes: json['notes'] as String,
  );
}

Map<String, dynamic> _$CityDataToJson(CityData instance) => <String, dynamic>{
      'notes': instance.notes,
      'active': instance.active,
      'confirmed': instance.confirmed,
      'deceased': instance.deceased,
      'recovered': instance.recovered,
      'delta': instance.delta,
    };

Delta _$DeltaFromJson(Map<String, dynamic> json) {
  return Delta(
    confirmed: json['confirmed'] as int,
    deceased: json['deceased'] as int,
    recovered: json['recovered'] as int,
  );
}

Map<String, dynamic> _$DeltaToJson(Delta instance) => <String, dynamic>{
      'confirmed': instance.confirmed,
      'deceased': instance.deceased,
      'recovered': instance.recovered,
    };

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _RestClient implements RestClient {
  _RestClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    this.baseUrl ??= 'https://api.covid19india.org/';
  }

  final Dio _dio;

  String baseUrl;

  @override
  getTasks() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/state_district_wise.json',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = MyCountry.fromJson(_result.data);
    return value;
  }
}
