import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'test_retro.g.dart';

@RestApi(baseUrl: "https://cat-fact.herokuapp.com/")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET("/facts/random?animal_type=cat&amount=5")
  Future<List<Source>> getData();
}

@JsonSerializable()
class Source {
  bool used;
  String source;
  String type;
  bool deleted;
  String id;
  String text;

  Status status;

  String user;

  Source({this.used,this.source,this.type,this.id,this.text,this.deleted,this.status,this.user});

  factory Source.fromJson(Map<String, dynamic> json) => _$SourceFromJson(json);
  Map<String, dynamic> toJson() => _$SourceToJson(this);
}

@JsonSerializable()
class Status{
  bool verified;
  int sentCount;

  Status({this.sentCount,this.verified});

  factory Status.fromJson(Map<String, dynamic> json) => _$StatusFromJson(json);
  Map<String, dynamic> toJson() => _$StatusToJson(this);
}
