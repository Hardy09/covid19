import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'Fetched_Data.g.dart';

@RestApi(baseUrl: "https://api.covid19india.org/")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET("/state_district_wise.json")
  Future<MyCountry> getTasks();
}

@JsonSerializable()
class MyCountry {

  @JsonKey(name:"Uttar Pradesh", required: true)
  MyState myState;

  MyCountry({this.myState});
  factory MyCountry.fromJson(Map<String, dynamic> json) => _$MyCountryFromJson(json);
  Map<String, dynamic> toJson() => _$MyCountryToJson(this);
}

@JsonSerializable()
class MyState {

  @JsonKey(name:"statecode", required: true)
  String stateCode;

  @JsonKey(name:"districtData",required: true)
  DistrictData data;

  MyState({this.stateCode,this.data});

  factory MyState.fromJson(Map<String, dynamic> json) => _$MyStateFromJson(json);
  Map<String, dynamic> toJson() => _$MyStateToJson(this);
}

@JsonSerializable()
class DistrictData{
  @JsonKey(name: "Agra")
  CityData Agra;

  @JsonKey(name: 'Aligarh')
  CityData aligarh ;

  @JsonKey(name: 'Ambedkar Nagar')
  CityData amber_nagar ;

  @JsonKey(name: 'Amethi')
  CityData amethi ;

  @JsonKey(name: 'Amroha')
  CityData amroha ;

  @JsonKey(name: 'Auraiya')
  CityData auriya ;

  @JsonKey(name: 'Ayodhya')
  CityData ayodhya ;

  @JsonKey(name: 'Azamgarh')
  CityData azamgarh ;

  @JsonKey(name: 'Baghpat')
  CityData bagpath ;

  @JsonKey(name: 'Balrampur')
  CityData balrampur ;

  @JsonKey(name: 'Banda')
  CityData banda ;

  @JsonKey(name: 'Barabanki')
  CityData barabanki ;

  @JsonKey(name: 'Bareilly')
  CityData bareilly ;

  @JsonKey(name: 'Basti')
  CityData basti ;

  @JsonKey(name: 'Bhadohi')
  CityData bhadohi ;

  @JsonKey(name: 'Bijnor')
  CityData bijnor ;

  @JsonKey(name: 'Budaun')
  CityData budaun ;

  @JsonKey(name: 'Bulandshahr')
  CityData bulandsahr ;

  @JsonKey(name: 'Chandauli')
  CityData chandauli ;

  @JsonKey(name: "Chitrakoot")
  CityData chitrakoot ;

  @JsonKey(name: "Etawah")
  CityData etawah ;

  @JsonKey(name: "Farrukhabad")
  CityData farrukhabad ;

  @JsonKey(name: "Firozabad")
  CityData firozabad ;

  @JsonKey(name: "Ghaziabad")
  CityData ghaziabad ;

  @JsonKey(name: "Gonda")
  CityData gonda ;

  @JsonKey(name: "Hapur")
  CityData hapur ;

  @JsonKey(name: "Kanpur Nagar")
  CityData kanpur ;

  @JsonKey(name: "Lucknow")
  CityData lucknow ;

  @JsonKey(name: "Meerut")
  CityData meerut ;

  @JsonKey(name: "Moradabad")
  CityData moradabad ;

  @JsonKey(name: "Pratapgarh")
  CityData pratapgarh ;

  @JsonKey(name: "Prayagraj")
  CityData prayagraj ;

  @JsonKey(name: "Rae Bareli")
  CityData raeBareli ;

  @JsonKey(name: "Rampur")
  CityData rampur ;

  @JsonKey(name: "Sitapur")
  CityData sitapur ;

  @JsonKey(name: "Unnao")
  CityData unnao ;

  @JsonKey(name: "Varanasi")
  CityData varanasi ;

  DistrictData({this.Agra,this.aligarh,this.amber_nagar,this.amethi,this.amroha,this.auriya,this.ayodhya,
  this.azamgarh,this.bagpath,this.balrampur,this.banda,this.barabanki,this.bareilly,this.basti,this.bhadohi,
  this.bijnor,this.budaun,this.bulandsahr,this.chandauli,this.chitrakoot,this.etawah,
  this.farrukhabad,this.firozabad,this.ghaziabad,this.gonda,this.hapur,this.kanpur,this.lucknow,this.meerut,
  this.moradabad,this.pratapgarh,this.prayagraj,this.raeBareli,this.rampur,this.sitapur,this.unnao,this.varanasi});

  factory DistrictData.fromJson(Map<String, dynamic> json) => _$DistrictDataFromJson(json);
  Map<String, dynamic> toJson() => _$DistrictDataToJson(this);

}

@JsonSerializable()
class CityData{
  String notes;
  int active;
  int confirmed;
  int deceased;
  int recovered;
  Delta delta;

  CityData({this.recovered,this.deceased,this.confirmed,this.active,this.delta,this.notes});
  factory CityData.fromJson(Map<String, dynamic> json) => _$CityDataFromJson(json);
  Map<String, dynamic> toJson() => _$CityDataToJson(this);
}

@JsonSerializable()
class Delta{
  int confirmed;
  int deceased;
  int recovered;

  Delta({this.confirmed,this.deceased,this.recovered});
  factory Delta.fromJson(Map<String, dynamic> json) => _$DeltaFromJson(json);
  Map<String, dynamic> toJson() => _$DeltaToJson(this);
}
